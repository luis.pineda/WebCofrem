<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Establecimientos extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'nit', 'razon_social', 'email', 'telefono','celular','estado',
    ];

	const ACTIVA = 'A';
	const INACTIVA = 'I';

    public function convenios()
    {
        return $this->hasMany(ConveniosEsta::class,'establecimiento_id','id');
    }


    public function sucursales()
	{
		return $this->hasMany(Sucursales::class,'establecimiento_id','id');
	}

}
