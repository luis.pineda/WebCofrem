<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Servicios extends Model implements AuditableContract
{
    //
    use Auditable;

	const CODIGO_SERVICIO_REGALO = "R";
	const CODIGO_SERVICIO_CUPO = "C";
	const CODIGO_SERVICIO_BONO = "B";

	const TEXT_SERVICIO_REGALO = "Regalo";
	const TEXT_SERVICIO_CUPO = "Cupo Rotativo";
	const TEXT_SERVICIO_BONO = "Bono Empresarial";

    public static $colorLabel = ["primary","success","info","warning","danger","dark","custom"];


	protected $fillable = [
		'codigo','descripcion','tipo'
	];


	public function tipotarjetas()
	{
		return $this->belongsToMany(TipoTarjeta::class, 'servicio_tipo_tarjeta');
	}


}
