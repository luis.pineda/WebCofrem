<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;

class PagaPlastico extends Model
{

	const ACTIVA = 'A';
	const INACTIVA = 'I';
}
