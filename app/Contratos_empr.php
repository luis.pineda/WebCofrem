<?php

	namespace creditocofrem;

	use Illuminate\Database\Eloquent\Model;

	class Contratos_empr extends Model {


		const FORMA_PAGO_EFECTIVO = "E";
		const FORMA_PAGO_CONSUMO= "C";

		protected $table = 'contratos_emprs';

		protected $fillable = [
			'n_contrato',
			'valor_contrato',
			'valor_impuesto',
			'fecha',
			'empresa_id',
			'n_tarjetas',
			'forma_pago',
			'pdf',
			'cons_mensual',
			'dias_consumo',
			'adminis_tarjeta_id',
			'fecha_creacion',
			'n_factura',
		];

		public function administracion() {
			return $this->belongsTo(AdminisTarjetas::class, 'adminis_tarjeta_id', 'id');
		}

		public function empresa() {
			return $this->belongsTo(Empresas::class, 'empresa_id', 'id');
		}

		public function detallesproductos(){
			return $this->hasMany(DetalleProdutos::class, 'contrato_emprs_id', 'id');
		}

	}
