<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;
use creditocofrem\DetalleProdutos;

class Tarjetas extends Model
{
    public static $numero_tarjeta = "9999999";

	const ESTADO_TARJETA_CREADA = "C";
	const ESTADO_TARJETA_ACTIVA = "A";
	const ESTADO_TARJETA_INACTIVA = "I";
	const ESTADO_TARJETA_ANULADA = "N";
    const ESTADO_TARJETA_DUPLICADO = "D";

    const CODIGO_SERVICIO_REGALO = "R";
	const CODIGO_SERVICIO_AFILIADO = "A";
    const CODIGO_SERVICIO_BONO = "B";

    public static $TEXT_RESULT_MONTO_SUPERADO = "monto superado";
    public static $TEXT_RESULT_TARJETA_NO_EXISTE = "Esta tarjeta no existe en nel inventario";
    public static $TEXT_RESULT_TARJETA_TIPO_INACTIVA = "No se puede asiganar servicios a un tarjeta que tiene un tipo de tarjeta Inactivo";
    public static $TEXT_RESULT_TARJETA_NO_SERVICIO = "Esta tarjeta no acepta este tipo de servicio";
    public static $TEXT_RESULT_FACTURA_Y_NUMTARJETA_EXISTEN = "ya existe un registro de esta factura para esta tarjeta";
    public static $TEXT_RESULT_FACTURA_Y_NUMTARJETA_EXISTENE_BLOQUE = "ya existe un registro de esta factura para esta tarjeta ";
    public static $TEXT_RESULT_FACTURA_Y_NUMTARJETA_EXISTENEN_BLOQUE = "ya existen registros de esta factura asosiados a estas tarjetas: ";
    public static $TEXT_CREACION_TARJETA_INGRESO_INVENTARIO = "Creación de la tarjeta - ingreso al inventario";
    public static $TEXT_BLOQUE_TARJETAS_YA_EXITEN = "Las siguientes tarjetas ya están registradas: ";
    public static $TEXT_BLOQUE_TARJETA_YA_EXISTE = "La siguiente tarjeta ya está registrada: ";

    public static $TEXT_SERVICIO_TARJETA_REGALO_SIN_PARAMETRIZACION = "Falta la parametrización del servicio Tarjeta regalo";
    public static $TEXT_SIN_VALOR_PLASTICO = "Falta asignarle un valor al plático de la tarjeta (parametrización)";

    public static $TEXT_DEFAULT_MOTIVO_ACTIVACION_TARJETA = 'Activacion del servicio';

    //
    protected $fillable = [
        'numero_tarjeta', 'cambioclave', 'password','persona_id','estado','tipo_tarjetas_id'
    ];


	/**
	 * Relacion logica de uno a muchos con entre tarjeta y detalle producto regalo
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function getDetalleProductoRegalo(){
        return $this->hasMany('creditocofrem\DetalleProdutos','numero_tarjeta','numero_tarjeta')->where('factura','<>',null);
    }

	/**
	 * Relacion logica de uno a muchos con entre tarjeta y detalle producto regalo
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function tarjetasRegalo(){
        return $this->hasMany(DetalleProdutos::class,"numero_tarjeta","numero_tarjeta")->where("factura","<>",null);
    }

	/*
    public function servicios(){
        return $this->belongsToMany(Servicios::class,'tarjeta_servicios','numero_tarjeta','servicio_codigo');
    }*/

	/**
	 * Relacion inversa de uno a uno entre tarjeta y tipo de tarjeta
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function tipo(){
    	return $this->belongsTo(TipoTarjeta::class,"tipo_tarjetas_id","id");
	}

	/**
	 * relacion de uno a muchos entre tarjetas y detalleproducto
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function detalleproducto(){
		return $this->hasMany(DetalleProdutos::class,'numero_tarjeta','numero_tarjeta');
	}

	/**
	 * relacion logica de uno a uno entre tarjeta y detalleproducto
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function detallep(){
		return $this->hasOne(DetalleProdutos::class,'numero_tarjeta','numero_tarjeta');
	}

	/**
	 * relacion logica de uno a uno entre persona y tarjeta
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function persona(){
		return $this->belongsTo(Personas::class,'persona_id','id');
	}

	/**
	 * relacion de uno a muchos entre tarjetas y h_tarjetas
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function htarjeta(){
    	return $this->hasMany(Htarjetas::class,'tarjeta_id','id');
	}

}
