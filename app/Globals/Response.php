<?php
	/**
	 * Created by PhpStorm.
	 * User: ceindetec
	 * Date: 29/08/18
	 * Time: 03:22 PM
	 */

	namespace creditocofrem\Globals;


	class Response {



		public static function responseSuccess($message,$code,$data){
			return response()->json([
				KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
				KeysResponse::KEY_MESSAGE => $message,
				KeysResponse::KEY_DATA    => $data,
			], $code);
		}

		public static function responseError($message,$code,$data = null){
			return response()->json([
				KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
				KeysResponse::KEY_MESSAGE => $message,
				KeysResponse::KEY_DATA    => $data,
			], $code);
		}

		public static function responseNotFound($message,$code,$data = null){
			return response()->json([
				KeysResponse::KEY_STATUS  => KeysResponse::STATUS_NOT_FOUND,
				KeysResponse::KEY_MESSAGE => $message,
				KeysResponse::KEY_DATA    => $data,
			], $code);
		}


	}