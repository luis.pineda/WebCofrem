<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;

class HEstadoTransaccion extends Model
{
	const ESTADO_ACTIVO = "A";
	const ESTADO_INACTIVO = "I";


    protected $table = 'h_estado_transacciones';
}
