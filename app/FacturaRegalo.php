<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;

class FacturaRegalo extends Model
{

	protected $fillable = [
		"tip_docu",
		"cli_coda",
		"cli_noco",
		"top_codi",
		"fac_cont",
		"tip_codi",
		"tip_nomb",
		"cxc_nume",
		"cxc_fech",
		"cxc_cont",
		"cxc_tota",
		"cxc_pago",
		"cxc_sald",
		"cxc_feve",
	];




}
