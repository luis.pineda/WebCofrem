<?php

	namespace creditocofrem\Http\Controllers;

	use creditocofrem\AdminisTarjetas;
	use creditocofrem\Contratos_empr;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\Globals\Response;
	use creditocofrem\Tarjetas;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Http\Request;
	use creditocofrem\Establecimientos;
	use Yajra\Datatables\Datatables;
	use creditocofrem\Empresas;
	use Illuminate\Support\Facades\Storage;
	use Carbon\Carbon;


	class ContratosController extends Controller {

		/**
		 * trae la vista donde se listan todas las empresas de la red cofrem
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index() {
			return view('empresas.contratos.listacontratos');
		}

		/**
		 * metodo que carga la grid, en donde se listan todos los contartos de la red cofrem
		 *
		 * @return retorna el arreglo de tal manera que el datatable lo entienda
		 */
		public function gridContratos() {
			//buscamos todos los contratos creados para las empresas
			$contratos = Contratos_empr::with("empresa")->get();

			//retornamos el listado de contratos en un formato que el datatable del cliente espera
			return Datatables::of($contratos)
							 ->addColumn('action', function ($contratos) {
								 $acciones = '<div class="btn-group">';
								 $acciones = $acciones.'<a href="'.route("contrato.editar", ["id" => $contratos->id])
									 .'" data-modal class="btn btn-xs btn-custom" ><i class="ti-pencil-alt"></i> Editar</a>';
								 $acciones = $acciones.'</div>';

								 return $acciones;
							 })
							 ->make(true);
		}

		/**
		 * retorna la vista del modal que permite crear nuevos contratos
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewCrearContrato() {

			//Buscamos la lista de de porcentajes de pago de administracion que hay para el pago de bono bono empresarial

			$administracion = AdminisTarjetas::query()
											 ->where('servicio_codigo', Tarjetas::CODIGO_SERVICIO_BONO)
											 ->where('estado', 'A')
											 ->orderBy("porcentaje", "asc")
											 ->pluck('porcentaje', 'id');

			//retornamos la vista para el modal de crear contrato junto con la data de administracion para el select
			return view('empresas.contratos.modalcrearcontratos', compact('administracion'));

		}

		public function crearContrato(Request $request) {

			\DB::beginTransaction();//inicamos una transacion en la base de datos para evitar datos huerfamos por posibles fallos en la aplicacion
			try {

				//validamos que noexista un contrato con elmismo numero de contrato
				$validator = \Validator::make($request->all(), [
					'n_contrato' => 'required|unique:contratos_emprs|max:11',
				], [
					'n_contrato.unique' => 'El contrato ya existe',
				]);

				if ($validator->fails()) {//de existir un numero de contrato devolvemos al cliente el error correspodiente
					return Response::responseError(MessageResponse::MESSAGE_CREATE_ERROR,CodesResponse::CODE_FORM_INVALIDATE,$validator->errors()->all());

				}

				//buscamos la empresa correspondiente al nit, para asociarle el contrato
				$empresa = Empresas::query()->where("nit", $request->nit)->first();

				if ($empresa != null) { // si existe la empresa creamos el cotrato

					$contrato = new Contratos_empr(); // instanciamos un modelo vacio del contrato
					$contrato->fill($request->all()); //inicalizamos los atributos del modelo contrato con los obtenidos por el request
					$ruta = "contratos_pdf/"; // ruda donde quedara guardado el pdf con los archivos del contrato

					$num_contrato    = $request->n_contrato; // guadamos el numero de contrato que traemos del request
					$new_name        = $ruta.$num_contrato.".pdf"; // creamos la ruta donde guardaremos el pdf
					$fichero         = $request->file('pdf'); // obtenemos el archivo que se envia desde el formulario
					$contrato->fecha = Carbon::createFromFormat('d/m/Y', $request->fecha); // creamos un formato de fehca valido con carbon
					// limpiamos los valores enviados del request del valor de contrato, elimnamos las comas solo era para presentacion
					$contrato->valor_contrato = str_replace('.', '', $request->valor_contrato);
					// limpiamos los valores enviados del request del valor de contrato, elimnamos las comas solo era para presentacion
					$contrato->valor_impuesto = str_replace('.', '', $request->valor_impuesto);

					$contrato->empresa_id = $empresa->id; // asociamos la empresa al contrato

					//si exite el archivo en el request creamos el archivo
					if ($fichero != null) {
						$nombre_file = $fichero->getClientOriginalName();
						$extensiones = explode(".", $nombre_file);
						$extension   = end($extensiones);
						if ($extension == "pdf") { //validamos que el archivo sea un pdf
							copy($_FILES['pdf']['tmp_name'], $new_name);
							$contrato->pdf = $new_name;

						}
						else {// de no ser un pdf retornamos un error al cliente
							return Response::responseError('El archivo debe tener extensión pdf',CodesResponse::CODE_BAD_REQUEST);
						}
					}

					$contrato->save();// guadamos los cambios del contrato
					\DB::commit();// guardamos los cambios en base de datos

					return Response::responseSuccess(MessageResponse::MESSAGE_CREATE_SUCCESS,CodesResponse::CODE_CREATED,$contrato);

				}
				else {// si no obtenemos ninguna empresa retornamos un error a al cliente

					return Response::responseError('El nit ingresado no corresponde a ninguna empresa',CodesResponse::CODE_BAD_REQUEST);

				}
			} catch (\Exception $exception) { // si ocurre algun error no contralado le retornamos al cliente
				\DB::rollBack(); // limpiamos los cambios en la base de datos

				return Response::responseError($exception->getMessage(),CodesResponse::CODE_INTERNAL_SERVER);

			}
		}




		public function validarFactura(Request $request){

			$empresa = Empresas::query()->where("nit",$request->nit)->first();
			$tip_docu = $empresa->tipo_documento;
			$cli_coda = $empresa->nit;

			$resulconsula = TarjetasController::consultarFacturaWS(406, 3928, $tip_docu, $cli_coda, 6064, $request->n_factura);


			if (!$resulconsula["estado"]) {
				return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
			}

			$factura = $resulconsula["factura"];

			if ($factura->cxc_sald != 0) {
				return Response::responseError(
					"No se puede crear el contrato por que No se ha cancelado la totalidad de la factura",
					CodesResponse::CODE_BAD_REQUEST);
			}

			//quitamos la mascara de pesos al monto
			$valorContrato = str_replace(".", "", $request->valor_contrato);

			if ($factura->cxc_tota < $valorContrato) {
				return Response::responseError(
					"No se puede crear el contrato.\n El valor del contrato supera el valor pagado en la factura",
					CodesResponse::CODE_BAD_REQUEST);
			}

			return Response::responseSuccess(MessageResponse::MESSAGE_QUERY_SUCCESS,CodesResponse::CODE_OK,$resulconsula["factura"]);

		}

		/**
		 * retorna ka vista para el modal de editar contrato
		 *
		 * @param \Illuminate\Http\Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewEditarContrato(Request $request) {
			try {

				$contrato       = Contratos_empr::query()->findOrFail($request->id); // consultamos el contrato a editar
				$administracion = AdminisTarjetas::pluck('porcentaje', 'id'); // traemos la lista de administraciones existentes
				$empresa        = Empresas::where('id', $contrato->empresa_id)->first(); //traemos la empresa que tiene asociado el contrato
				//retornamos la vista que cargaremos al modal junto con la data que necesita los selects
				return view('empresas.contratos.editarcontratos', compact('contrato', 'administracion', 'empresa'));

			} catch (\Exception $exception) {
				return response()->json($exception->getMessage(), CodesResponse::CODE_NOT_FOUND);
			}

		}

		/**
		 * metodo que permite editar un contrato
		 */
		public function editarContrato(Request $request, $id) {


			try {
				//consulta el contracto que se quiere editar
				$contrato = Contratos_empr::query()->findOrFail($id);

				// actualizamos los valores del contrato, por los enviados por el request
				$contrato->valor_contrato = $request->valor_contrato;
				$contrato->valor_impuesto = $request->valor_impuesto;
				$contrato->fecha          = Carbon::createFromFormat('d/m/Y', $request->fecha);
				$ruta                     = "contratos_pdf/";
				$num_contrato             = $request->n_contrato;
				$new_name                 = $ruta.$num_contrato.".pdf";
				$fichero                  = $request->file('pdf');
				$contrato->forma_pago     = $request->forma_pago;

				if ($fichero != null) {//preguntamos si fue enviado un fichero en el formulario, el cual actulizara el existente de exisitir
					$nombre_file = $fichero->getClientOriginalName();
					$extensiones = explode(".", $nombre_file);
					$extension   = end($extensiones);
					if ($extension == "pdf") { //validamos que el archivo tenga extencion pdf

						if (file_exists($contrato->pdf)) { // si el contrato ya tiene un pdf asignado, eliminamos el anterior
							unlink($contrato->pdf);
						}
						copy($_FILES['pdf']['tmp_name'], $new_name); // guardamos el pdf en el servidor
						$contrato->pdf = $new_name; // asignamos la ruta de pdf a guardar
					}
				}

				// limpiamos los valores enviados del request del valor de contrato, elimnamos las comas solo era para presentacion
				$contrato->valor_contrato = str_replace('.', '', $request->valor_contrato);
				// limpiamos los valores enviados del request del valor de contrato, elimnamos las comas solo era para presentacion
				$contrato->valor_impuesto = str_replace('.', '', $request->valor_impuesto);

				$contrato->save();//guardamos los cambios del contrato

				//regresamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_UPDATE_SUCCESS,
					KeysResponse::KEY_DATA    => $contrato,
				], CodesResponse::CODE_OK);

			}catch (ModelNotFoundException $e) {

			}
			catch (\Exception $exception) {
				//dado que ocurra un error en el sistema, retornamos una respuesta con el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * metodo para elcampo de autocompletar los nit de empresa
		 *
		 * @param \Illuminate\Http\Request $request
		 */
		public function autoCompleNitEmpresa(Request $request) {

			// buscamos los nits que cionsidad con la busqueda
			$nits = Empresas::query()
							->where("nit", "like", "%".$request["query"]."%")
							->get();

			//si no hay resultados devolvemos un array vacio
			if (count($nits) == 0) {
				$data["query"]       = "Unit";
				$data["suggestions"] = [];
			}
			else {
				// si hay resultado en la consultada oraganizamos el array por el esperado por el componente
				$arrayNits = [];
				foreach ($nits as $item) {
					$arrayNits[] = [
						"value" => $item->nit,
						"data"  => $item->nit,
					];
				}
				$data["suggestions"] = $arrayNits;
				$data["query"]       = "Unit";
			}

			return $data;
		}


		public function viewPdf($id) {
			return view('empresas.contratos.pdfcontrato');
		}
	}
