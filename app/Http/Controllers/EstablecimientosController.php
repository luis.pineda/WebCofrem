<?php

	namespace creditocofrem\Http\Controllers;

	use creditocofrem\ConveniosEsta;
	use creditocofrem\Establecimientos;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\Sucursales;
	use creditocofrem\Terminales;
	use Illuminate\Http\Request;
	use Yajra\Datatables\Datatables;

	class EstablecimientosController extends Controller {

		/**
		 * trae la vista donde se listan todos los establecimientos de la red cofrem
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index() {
			return view('establecimientos.listaestablecimientos');
		}

		/**
		 * metodo que carga la grid, en donde se listan todos los establecimientos de la red cofrem
		 *
		 * @return retorna el arreglo de tal manera que el datatable lo entienda
		 */
		public function gridEstablecimientos() {
			$establecimientos = Establecimientos::all();// consultamos todos los establecimientos creados

			return Datatables::of($establecimientos)
							 ->addColumn('action', function ($establecimientos) {
								 $acciones = '<div class="btn-group">';
								 $acciones = $acciones.'<a href="'.route("establecimiento.editar", ["id" => $establecimientos->id])
									 .'" class="btn btn-xs btn-custom" ><i class="ti-pencil-alt"></i> Edit</a>';
								 $acciones = $acciones.'<a class="btn btn-xs btn-primary" href="'.route("listsucursales", [$establecimientos->id])
									 .'"><i class="ti-layers-alt"></i> Sucursales</a>';
								 $acciones = $acciones.'</div>';

								 return $acciones;
							 })
							 ->make(true);
		}

		/**
		 * retorna la vista del modal que permite crear nuevos establecimientos
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewCrearEstablecimiento() {
			return view('establecimientos.modalcrearestablecimientos');
		}

		/**
		 * metodo que permite agregar un nuevo establecimiento al sistema
		 *
		 * @param Request $request trae la informacion del formulario, para agregar el establecimiento
		 *
		 * @return mixed returna una respuesta positiva o negativa dependiendo de la transaccion
		 */
		public function crearEstablecimiento(Request $request) {
			try {
				//validamos que el nit y el email sea unico el la tabla establecimientos
				$validator = \Validator::make($request->all(), [
					'nit'   => 'required|unique:establecimientos|max:11',
					'email' => 'required|unique:establecimientos',
				]);

				if ($validator->fails()) {//si no cumple con la validacion retorna un error al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_ERROR,
						KeysResponse::KEY_DATA    => $validator->errors()->all(),
					], CodesResponse::CODE_FORM_INVALIDATE);
				}
				$establecimiento               = new Establecimientos($request->all()); //instanciamos un nuevo objeto de clase Establecimiento
				$establecimiento->razon_social = strtoupper($establecimiento->razon_social); //cambiamos a mayusculas la razon social del establecimiento
				$establecimiento->estado       = Establecimientos::INACTIVA;
				$establecimiento->save();

				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $establecimiento,
				], CodesResponse::CODE_CREATED);
			} catch (\Exception $exception) {

				//si ocurre un error en el servidor retornamos al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * trae la vista para editar establecimiento asi como agregar contractos y editarlos
		 *
		 * @param Request $request trae la id del establecimiento a editar
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewEditarEstablecimiento($id) {
			//traemos el establecimiento que se desea editar
			$establecimiento = Establecimientos::query()->findOrFail($id);
			if ($establecimiento == null) {
				return redirect()->back();
			}

			return view('establecimientos.editarestablecimiento', compact('establecimiento'));
		}

		/**
		 * metodo que permite editar un establecimiento comercial
		 *
		 * @param Request $request campos del establecimiento a editar
		 *
		 * @return mixed
		 */
		public function editarEstablecimiento(Request $request) {
			try {
				//se consulta el establecimiento que se quiere editar

				$establecimiento = Establecimientos::with([
					'convenios' => function ($query) {
						$query->where('estado', ConveniosEsta::ACTIVA);
					}, 'sucursales.terminales'
				])
												   ->findOrFail($request->getQueryString());

				//validamos que el nit o el email sean unicos en la tabla establecimientos
				$validator = \Validator::make($request->all(), [
					'nit'   => 'required|max:11|unique:establecimientos,nit,'.$establecimiento->id,
					'email' => 'required|unique:establecimientos,email,'.$establecimiento->id,
				]);

				if ($validator->fails()) {//de presentar algun error en la validacion retornamos un error en el cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_ERROR,
						KeysResponse::KEY_DATA    => $validator->errors()->all(),
					], CodesResponse::CODE_FORM_INVALIDATE);
				}
				//se se desea cambiar el estado al establecimiento a activo, validamos que tenga un convenio activo
				//de no tenerlo no se le permite hacer el cambio de estado
				if ($request->estado == Establecimientos::ACTIVA) {
					$convenios = $establecimiento->convenios;
					if ($convenios->count() == 0) {// si no tiene un convenio retornamos un error al cliente
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => 'No es posible cambiar el estado de un establecimiento sin un convenio activo.',
							KeysResponse::KEY_DATA    => null,
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					$establecimiento->sucursales()->update([
						'estado' => Sucursales::INACTIVA,
					]);

					foreach ($establecimiento->sucursales as $sucursal)
					{
						$sucursal->terminales()->update([
							'estado' => Terminales::$ESTADO_TERMINAL_INACTIVA
						]);
					}
				}
				$establecimiento->update($request->all());//actualizamos el establecimiento

				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_UPDATE_SUCCESS,
					KeysResponse::KEY_DATA    => $establecimiento,
				], CodesResponse::CODE_OK);
			} catch (\Exception $exception) {
				//si ocurre un servidor en el servidor retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}
	}
