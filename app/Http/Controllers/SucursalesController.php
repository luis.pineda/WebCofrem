<?php

	namespace creditocofrem\Http\Controllers;

	use creditocofrem\Departamentos;
	use creditocofrem\Establecimientos;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\Municipios;
	use creditocofrem\Sucursales;
	use creditocofrem\Terminales;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Yajra\Datatables\Datatables;
	use Facades\creditocofrem\Encript;

	class SucursalesController extends Controller {

		/**
		 * trae la vista de la lista de sucursales que pertenecen a un establecimiento
		 *
		 * @param $id id del establecimiento
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index($id) {
			try {
				//se trae los datos del extablecimiento al que se le quiere agregar sucursales
				$establecimiento = Establecimientos::query()->findOrFail($id);
				return view('establecimientos.sucursales.listasucursales', compact('establecimiento'));
			} catch (ModelNotFoundException $exception) {
				return abort(CodesResponse::CODE_NOT_FOUND);
			}

		}

		/**
		 * metodo usuado para cargar la lista de surcursales en el datatable
		 *
		 * @param Request $request trae la id del establecimiento
		 *
		 * @return mixed
		 */
		public function gridSuscursales(Request $request) {
			//se consulta todos las sucursales del establecimiento
			$sucursales = Sucursales::with('getMunicipio')->where('establecimiento_id', $request->id)->get();

			//se retorna la lista de sucursales al datatable del cliente
			return Datatables::of($sucursales)
							 ->addColumn('action', function ($sucursales) {
								 $acciones = '<div class="btn-group">';
								 $acciones = $acciones.'<a href="'.route("sucursal.editar", ["id" => $sucursales->id])
									 .'" data-modal="modal-lg" class="btn btn-xs btn-custom" ><i class="ti-pencil-alt"></i> Edit</a>';
								 $acciones = $acciones.'<a class="btn btn-xs btn-primary" href="'.route("listterminales", [$sucursales->id])
									 .'"><i class="ti-layers-alt"></i> Terminales</a>';
								 $acciones = $acciones.'</div>';

								 return $acciones;
							 })
							 ->make(true);
		}

		/**
		 * trae la vista que se mostrara en el modal de crear sucursal
		 *
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewCrearSucursal(Request $request) {
			$establecimiento_id = $request->id;
			//consultamos todos los departamentos, para ser enviados al select del formulario
			$departamentos = Departamentos::pluck('descripcion', 'codigo');

			return view('establecimientos.sucursales.modalcrearsucursal', compact('establecimiento_id', 'departamentos'));
		}

		/**
		 * metodo que crea una sucursal
		 *
		 * @param Request $request datos que trae del formulario para crear sucursal
		 *
		 * @return array
		 */
		public function crearSucursal(Request $request) {
			try {
				$sucursal = new Sucursales();//instanciamos un objeto de del modelo Sucursales
				$sucursal->fill($request->all()); // inicializamos los atributos del objeto con lo que trae el request
				$sucursal->establecimiento_id = $request->getQueryString();// asociamos la sucursal con el establecimiento

				//armamos la direccion con los datos que trae el request
				$sucursal->direccion = $request->direccion;

				//consultamos los datos del establecimiento que se le esta creando la sucursal
				$establecimiento = Establecimientos::query()->find($request->getQueryString());

				if ($establecimiento->estado == Establecimientos::ACTIVA)// si el establecimiento esta en estado activo, creamos la sucursal en estado activa
				{
					$sucursal->estado = Sucursales::ACTIVA;
				}
				else // si esta en estado en inactivo la sucursal se creara en estado inactiva
				{
					$sucursal->estado = Sucursales::INACTIVA;
				}
				//encritamos la contrseña de la sucursal
				$sucursal->password = Encript::encryption($request->password);
				$sucursal->save();// insertamos el objeto en la base de datos

				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $sucursal,
				], CodesResponse::CODE_CREATED);

			} catch (\Exception $exception) {
				// si ocurre un error retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_CREATED);
			}
		}

		/**
		 * metodo que trae los marcadores para mostrar en google maps
		 *
		 * @param Request $request
		 *
		 * @return \Illuminate\Support\Collection
		 */
		public function getMarketSucursales(Request $request) {
			//se consulta todos los marcadores de las sucursales que hay para el establecimiento
			$sucursales = Sucursales::where('establecimiento_id', $request->id)->select(['latitud', 'longitud', 'nombre'])->get();

			//se retorna a la vista para ser pintados en el mapa
			return $sucursales;
		}

		/**
		 * metodo que trae la vista con la informacion de la sucursal a editar
		 *
		 * @param Request $request id de la sucursal a editar
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewEditarSucursal(Request $request) {
			try {
				//se consulta los datos de la sucursal que se quiere editar
				$sucursal = Sucursales::with('getMunicipio.getDepartamento')->findOrFail($request->id);
				$depar    = $sucursal->getMunicipio->getDepartamento; // buscamos el departamento de la sucursal a editar
				//listamos todos los departamentos para enviarlos al select del formulario
				$departamentos = Departamentos::pluck('descripcion', 'codigo');

				//retornamos la vista que sera cargada en elmodal editar sucursal, con los datos de la sucursal a editar
				return view('establecimientos.sucursales.modaleditarsucursal',
					compact(
						'sucursal',
						'departamentos',
						'depar'
					));
			} catch (ModelNotFoundException $exception) {
				return response()->json($exception->getMessage(), CodesResponse::CODE_NOT_FOUND);
			}
		}

		/**
		 * metodo que permite editar una socursal determinada
		 *
		 * @param Request $request tar los parametros que se quieren editar de la sucursal, incluyendo su id para identificarla
		 *
		 * @return array
		 */
		public function editarSucursal(Request $request) {

			DB::beginTransaction();//iniciamos una transaccion para evitar datos huerfanos en caso de un error
			try {
				//consutamos los datos de la sucursal con su relacion de establecimiento
				$sucursal = Sucursales::with('getEstablecimiento','terminales')->findOrFail($request->getQueryString());
				//construimos la direccion atravez de los datos obtenidos por el request
				$sucursal->direccion = $request->direccion;
				$sucursal->email     = strtolower($request->email); //pasamos a minusculas el email que trae el request
				$sucursal->telefono  = $request->telefono;
				$sucursal->contacto  = $request->contacto;
				if ($request->estado == 'A' && $sucursal->estado == Sucursales::INACTIVA) { //si el request trae como estado activa
					if ($sucursal->getEstablecimiento->estado == 'I') {// se pregunta si el establecimiento esta en estado inactivo
						//de esta en estado inacitva el establecimiento, no se le permite cambiar de estado a la sucursal y se retorna un error al cliente

						$result['estado']  = false;
						$result['mensaje'] = 'No es posible activar una sucursal de un establecimiento inactivo';

						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => 'No es posible activar una sucursal de un establecimiento inactivo',
							KeysResponse::KEY_DATA    => null,
						], CodesResponse::CODE_BAD_REQUEST);
					}
					else {//si el establecimiento esta en estado activo se le permite cambiar de estado a la sucursal
						$sucursal->estado = $request->estado;
					}
				}
				else {// si se quire cambia a estado inactivo pasa derecho
					$sucursal->estado = $request->estado;
				}

				if ($request->estado == Sucursales::INACTIVA) {// si se va cambial el estado de la sucursal a inactivo, es necesario inactivar sus terminales
					$sucursal->terminales()->update([
						'estado' => Terminales::$ESTADO_TERMINAL_INACTIVA
					]);
				}

				if (trim($request->password) != "") {//si se envia una nueva contraseña para la sucursal, se remplaza
					$sucursal->password = Encript::encryption(trim($request->password));
				}
				$sucursal->save(); // actualizamos la sucursal
				DB::commit();// guardamos los cambios en la base de datos
				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_UPDATE_SUCCESS,
					KeysResponse::KEY_DATA    => $sucursal,
				], CodesResponse::CODE_OK);

			} catch (\Exception $exception) {
				DB::rollBack(); // de ocurrir un error, descartamos los cambios en la base de datos
				//retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_OK);
			}
		}
	}
