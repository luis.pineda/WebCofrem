<?php

	namespace creditocofrem\Http\Controllers;

	use Carbon\Carbon;
	use creditocofrem\AdminisTarjetas;
	use creditocofrem\CupoMinimo;
	use creditocofrem\DetalleProdutos;
	use creditocofrem\DetalleTransaccion;
	use creditocofrem\Duplicado;
	use creditocofrem\DuplicadoProductos;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\FacturaRegalo;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\Globals\Response;
	use creditocofrem\Htarjetas;
	use creditocofrem\Motivo;
	use creditocofrem\PagaPlastico;
	use creditocofrem\Servicios;
	use creditocofrem\Tarjetas;
	use creditocofrem\TarjetaServicios;
	use creditocofrem\TipoTarjeta;
	use creditocofrem\Transaccion;
	use creditocofrem\ValorTarjeta;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Auth;
	use SoapClient;
	use SoapFault;
	use Yajra\Datatables\Datatables;
	use Facades\creditocofrem\Encript;
	use Illuminate\Support\Facades\DB;

	class TarjetasController extends Controller {

		/**
		 * Trae la vista del inventario de tarjetas
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index() {

			// traigo los itpos de tarjetas con el total de tarjetas que tiene creada
			// mas las tarjetas que han sido asignadas y las que no

			$tiposTarjetas = TipoTarjeta::query()
										->withCount('tarjetas as total_tarjetas')
										->withCount([
											'tarjetas as total_asiganadas' => function ($query) {
												$query->has('detalleproducto');
											},
										])
										->withCount([
											'tarjetas as total_sinasignar' => function ($query) {
												$query->doesntHave('detalleproducto');
											},
										])
										->where('estado', TipoTarjeta::ESTADO_ACTIVO)
										->get();

			//retornamos a la vista con la data correspondientes a las carts que me contavilizan los tipos de tarjetas
			return view('tarjetas.listatarjetas',
				compact(
					'tiposTarjetas'
				));
		}

		/**
		 * Carga la grid donde se listan todas las tarjetas que hay en el inventario
		 *
		 * @return mixed
		 * @throws \Exception
		 */
		public function gridTarjetas() {
			/* traigo todas las tarjetas con la relacion del tipo de tarjeta mas el detalle productor
				para saber si la tarjeta ya cuenta con un detalle y determinar si puedo editarla o no */
			$tarjetas = Tarjetas::with("tipo")->withCount("detalleproducto");

			//retorno la data a la vista en el formato esperado por el datagrid
			return Datatables::of($tarjetas)
							 ->editColumn("estado", function ($tarjetas) {
								 $estado = "";
								 switch ($tarjetas->estado) {
									 case Tarjetas::ESTADO_TARJETA_DUPLICADO:
										 $estado = "Duplicado";
										 break;
									 case Tarjetas::ESTADO_TARJETA_INACTIVA:
										 $estado = "Inactiva";
										 break;
									 case Tarjetas::ESTADO_TARJETA_ACTIVA:
										 $estado = "Activa";
										 break;
									 case Tarjetas::ESTADO_TARJETA_ANULADA:
										 $estado = "Anulada";
										 break;
									 case Tarjetas::ESTADO_TARJETA_CREADA:
										 $estado = "Creada";
										 break;
								 }

								 return $estado;
							 })
							 ->addColumn('action', function ($tarjetas) {
								 $acciones = "";
								 if ($tarjetas->detalleproducto_count == 0 && $tarjetas->estado == Tarjetas::ESTADO_TARJETA_CREADA) { //si la tarjeta no tiene detalle producto la puedo editar
									 $acciones = '<a href="'.route('tarjetas.editar', ['id' => $tarjetas->id])
										 .'" data-modal="modal" class="btn btn-xs btn-custom" ><i class="glyphicon glyphicon-edit"></i> Editar</a>';
								 } else { //si la tarjeta tiene detalle producto no permito editarla
									 $acciones = "<span class='label label-warning'>No Editable</span>";
								 }
								 $acciones .= '<a href="'.route('tarjetas.historial', ['id' => $tarjetas->id])
									 .'" data-modal="modal-lg" class="btn btn-xs btn-info" ><i class="ti-pulse"></i> Historial</a>';

								 return $acciones;
							 })
							 ->make(true);
		}

		/**
		 * retorna el modal para crear una tarjeta
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewCrearTarjeta() {
			//consulto los tipos de tarjetas que hay creados y que tengan estado activo
			$tipo_tarjetas = TipoTarjeta::query()->where('estado', TipoTarjeta::ESTADO_ACTIVO)->pluck('nombre', 'id');

			return view('tarjetas.modalcreartarjetas', compact('tipo_tarjetas'));
		}

		/**
		 * Procesa la informacion enviada por el formulario de creacion de tarjeta individual
		 *
		 * @param \Illuminate\Http\Request $request
		 *
		 * @return array
		 */
		public function crearTarjeta(Request $request) {
			$result = [];
			DB::beginTransaction(); // iniciamos una transacion en la base de datos para evitar datos huerfanos
			try {
				$tarjetas = new Tarjetas(); // instanciamos un modelo vasio de tarjetas
				$tarjetas->fill($request->all()); // inicalizamos el modelo tarjeta con los datos enviados por el

				//la tarjeta debe tener 6 digitos, de hacer falta se rellenan con cero
				$codigo = $this->completarCeros($tarjetas->numero_tarjeta); // Obtenemos el numero de tarjeta que fue ingresado

				$tarjetas->numero_tarjeta = $codigo;//actualizamos el valor del numero tarjeta

				//Validamos que el numero de tarjeta que estamos tratando de crear sea unico
				$validator = \Validator::make(['numero_tarjeta' => $codigo], [
					'numero_tarjeta' => 'required|unique:tarjetas',
				]);

				if ($validator->fails()) { // si el numero de tarjeta ya exite regresamos un error al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_ERROR,
						KeysResponse::KEY_DATA    => $validator->errors()->all(),
					], CodesResponse::CODE_FORM_INVALIDATE);
				}

				$ultimos = substr($tarjetas->numero_tarjeta, -4); // obtenemos los ultimos 4 digitos del numero de tarjeta

				$tarjetas->password =
					Encript::encryption($ultimos); // los digitos obtenidos anteriormente los encriptamos y usamos como contraseña inicial de la tarjeta
				$tarjetas->estado   = Tarjetas::ESTADO_TARJETA_CREADA; // agregamos el estado inicial de la tarjeta como tarjeta creada

				$tarjetas->save(); // guardamos la tarjeta

				$data = $this->crearHtarjeta($tarjetas->id, Motivo::CODIGO_M_CREACION, "", Tarjetas::ESTADO_TARJETA_CREADA);
				if ($data['estado']) {
					DB::commit();

					return Response::responseSuccess(
						'La tarjeta ha sido creada satisfactoriamente',
						CodesResponse::CODE_OK,
						null);

				} else {
					\DB::rollBack();

					return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
				}

			} catch (\Exception $exception) {
				return Response::responseError('No fue posible crear la tarjeta '.$exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);
			}

		}

		//metodo para llamar a la vista modal para editar la info de la tarjeta
		public function viewEditarTarjeta(Request $request) {
			$tarjeta       = Tarjetas::with("tipo")->findOrFail($request->id);
			$tipo_tarjetas = TipoTarjeta::query()
										->where('estado', TipoTarjeta::ESTADO_ACTIVO)
										->pluck('nombre', 'id');

			return view('tarjetas.modaleditartarjeta', compact('tarjeta', 'tipo_tarjetas'));
		}

		//metodo para editar sólo el campo tipo, de la tarjeta
		public function editarTarjeta(Request $request, $id) {
			$result = [];
			DB::beginTransaction();
			try {
				$codigo = $this->completarCeros($request->numero_tarjeta);

				$numeroExiste = Tarjetas::query()->where('numero_tarjeta', $codigo)->whereNotIn('id', [$id])->first();
				//dd($dproducto);
				if ($numeroExiste == null) {

					$tarjeta                   = Tarjetas::query()->find($id);
					$tarjeta->numero_tarjeta   = $codigo;
					$tarjeta->tipo_tarjetas_id = $request->tipo_tarjetas_id;

					if ($tarjeta->isDirty()) {
						$tarjeta->save();
						$result['mensaje'] = 'La tarjeta ha sido actualizada satisfactoriamente.';
					} else {
						$result['mensaje'] = 'No hay cambios para realizar';
					}

					$result['estado'] = true;

					DB::commit();
				} else {
					$result['estado']  = false;
					$result['mensaje'] = 'No es posible actualizar la tarjeta, este número ya esta asignado a otra tarjeta';
					//$result['data'] = $tarjeta;
					DB::rollBack();
				}
			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible editar la tarjeta. '.$exception->getMessage();
				DB::rollBack();
			}

			return $result;
		}


		//metodo para llamar a la vista modal para crear tarjetas en bloque - masivo
		public function viewCrearTarjetaBloque() {

			$tipo_tarjetas = TipoTarjeta::query()->where('estado', TipoTarjeta::ESTADO_ACTIVO)->pluck('nombre', 'id');

			return view('tarjetas.modalcreartarjetabloque', compact('tipo_tarjetas'));
		}


		/**
		 * metodo para agregar  tarjetas en bloque: insertar en bd de forma masiva
		 *
		 * @param \Illuminate\Http\Request $request
		 *
		 * @return array
		 */
		public function crearTarjetaBloque(Request $request) {
			$result = [];
			DB::beginTransaction();
			try {

				$total      = $request->cantidad;
				$primer_num = $request->numero_primer_tarjeta;

				$arrayNumTarjetas = [];

				for ($i = $primer_num; $i < ($primer_num + $total); $i++) {

					$arrayNumTarjetas[] = $this->completarCeros($i);

				}

				$tarjetasExistentes = Tarjetas::query()->whereIn("numero_tarjeta", $arrayNumTarjetas)->pluck("numero_tarjeta");

				if ($tarjetasExistentes->count() == 0) {

					foreach ($arrayNumTarjetas as $num_tarjeta) {

						$ultimos = substr($num_tarjeta, -4);

						$tarjeta = new Tarjetas();
						$tarjeta->fill([
							"numero_tarjeta"   => $num_tarjeta,
							"password"         => Encript::encryption($ultimos),
							"tipo_tarjetas_id" => $request->tipo_tarjetas_id,
							"estado"           => Tarjetas::ESTADO_TARJETA_CREADA,
						]);

						$tarjeta->save();

						$data = $this->crearHtarjeta($tarjeta->id, Motivo::CODIGO_M_CREACION, "", Tarjetas::ESTADO_TARJETA_CREADA);
						if (!$data['estado']) {
							\DB::rollBack();

							return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
						}
					}

					DB::commit();

					return Response::responseSuccess(
						$total.' tarjetas han sido creadas satisfactoriamente',
						CodesResponse::CODE_OK,
						null);

				} else {

					if ($tarjetasExistentes->count() == 1) {
						$mstarjetas    = "El número de  tarjeta";
						$mstarjetasfin = " ya esta asignado a otra tarjeta";
					} else {
						$mstarjetas    = "Los números de tarjetas";
						$mstarjetasfin = " ya estan asignados a otras tarjetas";
					}

					$error = $mstarjetas." ".$tarjetasExistentes->implode(", ").$mstarjetasfin;

					return Response::responseError($error, CodesResponse::CODE_BAD_REQUEST);
				}

			} catch (\Exception $exception) {
				DB::rollBack();

				return Response::responseError('No fue posible crear la tarjeta '.$exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);
			}

		}

		/**
		 * metodo que trae la vista de gestiononar un detalle producto
		 *
		 * @param $id id del detalle producto a gestionar
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function gestionarTarjeta($id) {
			$detalleproducto = DetalleProdutos::find($id);

			return view('tarjetas.modalgestionartarjeta', compact('detalleproducto'));
		}

		/**
		 * metodo que trae la vista para duplicar tarjetas
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewDuplicarTarjeta() {
			return view('tarjetas.duplicadotarjeta');
		}

		/**
		 * trae la informacion de todas las tarjetas en la grid, para luego duplicar la que se necesite
		 *
		 * @return mixed
		 */
		public function gridTarjetasDuplicar() {
			//consultamos las tarjetas que esten creadas en el sistema y que esten en estado activas
			$tarjetas = Tarjetas::with('tipo')->where('estado', Tarjetas::ESTADO_TARJETA_ACTIVA)->get();

			//se envia al datatable de la vista cliente
			return Datatables::of($tarjetas)
							 ->addColumn('action', function ($tarjetas) {
								 return '<a href="'.route('tarjetas.modalduplicar', $tarjetas->id).'" data-modal class="btn btn-xs btn-custom">Duplicar</a>';
							 })
							 ->make(true);
		}

		/**
		 * trae la vista para el modal del formulario para duplicar una tarjeta
		 *
		 * @param $id id de la tarjeta que se quiere sacar un duplicado
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewModalDuplicarTarjeta($id) {
			try {
				//consultamos la tarjeta que queremos duplicar
				$tarjeta = Tarjetas::with('tipo')->findOrFail($id);
				$motivos = Motivo::query()->where('tipo', Motivo::MOTIVO_TIPO_DUPLICADO)->pluck('motivo', 'codigo');

				//enviamos la vista que sera cargada en el modal, junto con las variables necesarias por ellas
				return view('tarjetas.modalduplicartarjeta', compact('tarjeta', 'motivos'));
			} catch (\Exception $exception) {
				//si ocurre un error retornamos un error al cliente
				return response()->json($exception->getMessage(), CodesResponse::CODE_NOT_FOUND);
			}
		}

		/**
		 * metodo para el autocomplete del formulario en el campo numero de tarjeta
		 *
		 * @param Request $request
		 *
		 * @return mixed
		 */
		public function autoCompleteTarjetaDuplicado(Request $request) {

			$tarjetas = Tarjetas::query()
								->whereHas('tipo', function ($query) use ($request) {
									$query->where('id', $request->id);
								})
								->where("numero_tarjeta", "like", "%".$request["query"]."%")
								->where('estado', 'C')->get();

			if (count($tarjetas) == 0) {
				$data["query"]       = "Unit";
				$data["suggestions"] = [];
			} else {
				$arrayTarjetas = [];
				foreach ($tarjetas as $producto) {
					$arrayTarjetas[] = [
						"value" => $producto->numero_tarjeta,
						"data"  => $producto->id,
					];
				}
				$data["suggestions"] = $arrayTarjetas;
				$data["query"]       = "Unit";
			}

			return $data;
		}

		/**
		 * metodo que procesa los datos del formulario para duplicar una tarjeta
		 *
		 * @param Request $request datos referente a la nueva tarjeta
		 * @param         $id      id de la tarjeta que se quire duplicar
		 *
		 * @return array
		 */
		public function duplicarTarjeta(Request $request, $id) {
			$result = [];
			DB::beginTransaction();
			$olTarjeta  = Tarjetas::with("tipo.servicios")->find($id);
			$newTarjeta = Tarjetas::with("tipo.servicios")->where('numero_tarjeta', $request->numero_tarjeta)->first();
			if ($newTarjeta == null) {

				return Response::responseError('El numero de tarjeta no existe en el inventario',
					CodesResponse::CODE_BAD_REQUEST);

			}

			if ($newTarjeta->estado != Tarjetas::ESTADO_TARJETA_CREADA) {
				return Response::responseError('No se puede duplicar la tarjeta por que la nueva tarjeta ya se encuentra en uso',
					CodesResponse::CODE_BAD_REQUEST);
			}

			//validamos que el tipo de la tarjeta neva sea el mismo de lata darjeta vieja
			if ($olTarjeta->tipo->id != $newTarjeta->tipo->id) {

				return Response::responseError("No se puede duplicar la tarjeta por que nueva tarjeta no es del mismo tipo de tarjeta",
					CodesResponse::CODE_BAD_REQUEST);

			}

			try {

				$duplicado             = new Duplicado();
				$duplicado->oldtarjeta = $olTarjeta->numero_tarjeta;
				$duplicado->newtarjeta = $newTarjeta->numero_tarjeta;
				$duplicado->fecha      = Carbon::now();
				$duplicado->save();

				DetalleProdutos::query()->where("numero_tarjeta", $olTarjeta->numero_tarjeta)->update(["numero_tarjeta" => $newTarjeta->numero_tarjeta]);

				Transaccion::query()->where("numero_tarjeta", $olTarjeta->numero_tarjeta)->update(["numero_tarjeta" => $newTarjeta->numero_tarjeta]);

				$newTarjeta->estado = $olTarjeta->estado;
				$newTarjeta->save();

				$olTarjeta->estado = Tarjetas::ESTADO_TARJETA_DUPLICADO;
				$olTarjeta->save();


				$motivo = Motivo::query()->where('codigo', $request->motivo)->first();

				$data = $this->crearHtarjeta($olTarjeta->id, $request->motivo, "", Tarjetas::ESTADO_TARJETA_DUPLICADO,null,$request->nota);
				if (!$data['estado']) {
					\DB::rollBack();

					return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
				}

				$data = $this->crearHtarjeta($newTarjeta->id, Motivo::CODIGO_M_INICIALIZACION_POR_DUPLICADO, "", $newTarjeta->estado,null,$request->nota);
				if (!$data['estado']) {
					\DB::rollBack();

					return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
				}

				DB::commit();

				return Response::responseSuccess(
					'Duplicado de tarjeta creado satisfactoriamente',
					CodesResponse::CODE_OK,
					null);

			} catch (\Exception $exception) {
				DB::rollBack();

				return Response::responseError('Error durante la operacion '.$exception->getMessage(),
					CodesResponse::CODE_BAD_REQUEST);
			}

		}

		/**
		 * metodo encargado de colocar ceros la inicia del numero para completar la cantidad de caracteres necesrios
		 *
		 * @param $numero
		 * @param $digitos
		 *
		 * @return string
		 */
		public static function completarCeros($numero, $digitos = 6) {

			$num_tarjeta = $numero;
			while (strlen($num_tarjeta) < $digitos) {
				$num_tarjeta = "0".$num_tarjeta;
			}

			return $num_tarjeta;
		}


		/**
		 * Metodo que se encarga de validar si el servicio debe pagar platico
		 *
		 * @return mixed
		 */
		public static function validarRegaloPagaPlastico($servicio_codigo) {

			$pagaPlastico = PagaPlastico::query()
										->where("servicio_codigo", $servicio_codigo)
										->orderBy("id", "DESC")
										->first();

			if ($pagaPlastico != null) {

				if ($pagaPlastico->pagaplastico == 1) {

					$valorTarjeta = ValorTarjeta::query()->orderBy("id", "DESC")->first();

					if ($valorTarjeta != null) {
						$data["estado"] = true;
						$data["paga"]   = true;
						$data["valor"]  = $valorTarjeta->valor;
					} else {
						$data["estado"]  = false;
						$data["mensaje"] = Tarjetas::$TEXT_SIN_VALOR_PLASTICO;
					}

				} else {
					$data["estado"] = true;
					$data["paga"]   = false;
				}

			} else {
				$data["estado"]  = false;
				$data["mensaje"] = Tarjetas::$TEXT_SERVICIO_TARJETA_REGALO_SIN_PARAMETRIZACION;
			}

			return $data;
		}

		/**
		 * Metodo que se encarga de validar si el servicio debe pagar Administracion
		 *
		 * @return mixed
		 */
		public static function validarRegaloPagaAdmon($servicio_codigo) {

			$porcentajeAdmon = AdminisTarjetas::query()->where("servicio_codigo", $servicio_codigo)
											  ->orderBy("id", "DESC")
											  ->first();

			if ($porcentajeAdmon != null) {

				if ($porcentajeAdmon->estado == Tarjetas::ESTADO_TARJETA_ACTIVA) {

					$data["estado"] = true;
					$data["paga"]   = true;
					$data["valor"]  = $porcentajeAdmon->porcentaje;

				} else {
					$data["estado"] = true;
					$data["paga"]   = false;
				}

			} else {
				$data["estado"]  = false;
				$data["mensaje"] = Tarjetas::$TEXT_SERVICIO_TARJETA_REGALO_SIN_PARAMETRIZACION;
			}

			return $data;
		}

		public static function validarCupoMinimo($servicio_codigo) {

			$cupo = CupoMinimo::query()
							  ->where("servicio_codigo", $servicio_codigo)
							  ->where("estado", CupoMinimo::ACTIVO)
							  ->first();

			if ($cupo != null) {
				$data["estado"] = true;
				$data["valor"]  = $cupo->cupominimo;
			} else {
				$data["estado"]  = false;
				$data["mensaje"] = Tarjetas::$TEXT_SERVICIO_TARJETA_REGALO_SIN_PARAMETRIZACION;
			}

			return $data;

		}


		public static function consultarFacturaWS($emp_codi, $tip_codi, $tip_docu, $cli_coda, $top_codi, $cxc_nume) {

			try {
				$url = "http://192.168.0.188/WebServices4/SEVENConsultingServicesCA.asmx?WSDL";

				$soap = new soapclient($url, [
					'trace'      => true,
					'exceptions' => true,
					"location"   => "http://192.168.0.188/WebServices4/SEVENConsultingServicesCA.asmx",
					'cache_wsdl' => WSDL_CACHE_NONE,
				]);

				$parameters = [
					"info" => [
						"emp_codi" => $emp_codi,
						"tip_codi" => $tip_codi,
						"tip_docu" => $tip_docu,
						"cli_coda" => $cli_coda,
						"top_codi" => $top_codi,
						"cxc_nume" => $cxc_nume,
					],
				];

				$resultWS = $soap->ConsultarCarteraPorTipoProducto($parameters);

				if ($resultWS->ConsultarCarteraPorTipoProductoResult->retorno == 0) {

					$factws = $resultWS->ConsultarCarteraPorTipoProductoResult->objResult->facturas->CA_CXCOB_OUT;

					$facturaRagalo = FacturaRegalo::query()->where("cxc_nume", $factws->cxc_nume)->first();

					if ($facturaRagalo == null) {
						$facturaRagalo = new FacturaRegalo();
					}

					$facturaRagalo->fill([
						"tip_docu" => $resultWS->ConsultarCarteraPorTipoProductoResult->objResult->tip_codi,
						"cli_coda" => $resultWS->ConsultarCarteraPorTipoProductoResult->objResult->cli_coda,
						"cli_noco" => $resultWS->ConsultarCarteraPorTipoProductoResult->objResult->cli_noco,
						"top_codi" => $factws->top_codi,
						"fac_cont" => $factws->fac_cont,
						"tip_codi" => $factws->tip_codi,
						"tip_nomb" => $factws->tip_nomb,
						"cxc_nume" => $factws->cxc_nume,
						"cxc_fech" => $factws->cxc_fech,
						"cxc_cont" => $factws->cxc_cont,
						"cxc_tota" => $factws->cxc_tota,
						"cxc_pago" => $factws->cxc_pago,
						"cxc_sald" => $factws->cxc_sald,
						"cxc_feve" => $factws->cxc_feve,
					]);

					$facturaRagalo->save();

					$result["estado"]  = true;
					$result["factura"] = $facturaRagalo;

					//					dd($result);
					//

				} else {
					$result["estado"]  = false;
					$result["mensaje"] = "Error: ".$resultWS->ConsultarCarteraPorTipoProductoResult->txtError;
				}


			} catch (SoapFault $e) {
				$result["estado"]  = false;
				$result["mensaje"] = "Error: ".$e->getMessage();
			}

			return $result;

		}

		/**
		 * Metodo que trae la vista que se carga el el modal del historial de la tarjeta
		 *
		 * @param                          $id
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
		 */
		public function historialTarjeta($id) {
			try {
				$tarjeta = Tarjetas::query()->findOrFail($id);

				return view('tarjetas.madalhistorialtarjeta', compact('tarjeta'));
			} catch (\Exception $exception) {
				return response()->json($exception->getMessage(), CodesResponse::CODE_NOT_FOUND);
			}
		}


		public function gridHistorialTarjeta($id) {
			$historial = Htarjetas::with('motivo')->where('tarjetas_id', $id)->orderBy('fecha', 'desc');

			return Datatables::of($historial)
							 ->editColumn('servicio_codigo', function ($historial) {
								 $servicio = "No aplica";
								 switch ($historial->servicio_codigo) {
									 case 'R':
										 $servicio = 'Regalo';
										 break;
									 case 'B':
										 $servicio = 'Bono Empresarial';
										 break;
									 case 'A':
										 $servicio = 'Cupo Rorativo';
										 break;

								 }

								 return $servicio;
							 })
							 ->editColumn('estado', function ($historial) {
								 switch ($historial->estado) {
									 case 'A':
										 $servicio = 'Activa';
										 break;
									 case 'C':
										 $servicio = 'Creada';
										 break;
									 case 'I':
										 $servicio = 'Inactiva';
										 break;
									 case 'D':
										 $servicio = 'Duplicada';
										 break;
									 case 'N':
										 $servicio = 'Anulada';
										 break;
								 }

								 return $servicio;
							 })
							 ->editColumn('estado_producto', function ($historial) {

								 if ($historial->estado_producto == null) {
									 $estado = "No aplica";
								 } else {
									 switch ($historial->estado_producto) {
										 case 'I':
											 $estado = 'Inactivo';
											 break;
										 case 'A':
											 $estado = 'Activo';
											 break;
										 case 'N':
											 $estado = 'Anulado';
											 break;
									 }

									 return $estado;
								 }

								 return $estado;
							 })
							 ->make(true);

		}

	}
