<?php

	namespace creditocofrem\Http\Controllers;

	use creditocofrem\AdminisTarjetas;
	use creditocofrem\CuenContaTarjeta;
	use creditocofrem\CupoMinimo;
	use creditocofrem\Departamentos;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\PagaPlastico;
	use creditocofrem\Servicios;
	use creditocofrem\Tarjetas;
	use creditocofrem\TipoTarjeta;
	use creditocofrem\ValorTarjeta;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Yajra\Datatables\Facades\Datatables;

	class ParametrizacionTarjetasController extends Controller {

		//

		/**
		 * metodo que trae la vista para parametrizar las tarjetas
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewParametrosTarjetas() {
			//consulto todos los tipos de servicios que hay creados
			$tipotarjetas = Servicios::all()->pluck('descripcion', 'codigo');
			$valorTarjeta = ValorTarjeta::where('estado', ValorTarjeta::ACTIVA)->first(); //se obtine el ultimo valor definido para el platico
			if ($valorTarjeta != null) {
				if (!strrpos($valorTarjeta->valor, '.')) {
					$valorTarjeta->valor = $valorTarjeta->valor.',00'; // si hay valor de platico, se maquilla para la vista
				}
			}

			//se renorna la vista principal de pra metrizacion con las obtenidas anteriormente para llenar selects y campos necesarios
			return view('tarjetas.parametrizacion.parametrizacion', compact('tipotarjetas', 'valorTarjeta'));
		}

		/**
		 * metodo que permite agregar un valor al plastico de la tarjeta
		 *
		 * @param Request $request
		 *
		 * @return array
		 */
		public function tarjetaCrearParametroValor(Request $request) {
			$result = [];
			\DB::beginTransaction();// inicalizamos una transacion en la base de datos
			try {
				$existe   = ValorTarjeta::query()->exists();//preguntamos si existe una parametrizacion previa al valor de la tarjeta
				$newvalor = new ValorTarjeta(); // instanciamos un modelo vacio de ValorTarjeta
				if ($existe) {//de existir una parametizacion anterior cambiamos de estado, y actualizamos el valor del modelo nuevo
					ValorTarjeta::where('estado', ValorTarjeta::ACTIVA)->update(['estado' => ValorTarjeta::INACTIVA]);
					$newvalor->valor  = str_replace(",", ".", str_replace(".", "", $request->valor));
					$newvalor->estado = ValorTarjeta::ACTIVA;
				}
				else {//de no exisitir parametrizacion antterior, inicializamos el modelo nuevo
					$newvalor->valor  = str_replace(",", ".", str_replace(".", "", $request->valor));
					$newvalor->estado = ValorTarjeta::ACTIVA;
				}
				$newvalor->save();// guadamos los cambios del modelo
				\DB::commit(); // insertamos los cambios en la base de datos
				$result['estado']  = true;
				$result['mensaje'] = 'Valor ingresado satisfactoriamente';
			} catch (\Exception $exception) {
				\DB::rollback(); // de ocurrir un error descartamos los cambios en la base de datos
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible ingresar el valor '.$exception->getMessage();
			}

			return $result;
		}

		/**
		 * permite agregar valores de administracion para las tarjetas
		 *
		 * @param Request $request
		 *
		 * @return array
		 */
		public function tarjetaCrearParametroAdministracion(Request $request, $codigo) {
			try {
				$existe = AdminisTarjetas::query()->exists(); // se consulta si exite una administracion creada para este servicio
				if ($existe) {// de existir, buscamos las administraciones existentes para ver si hay una administracion igual a la que se quiere crear
					$oldAdministraciones = AdminisTarjetas::query()
														  ->where('estado', AdminisTarjetas::ESTADO_ACTIVO)
														  ->where('servicio_codigo', $codigo)
														  ->get();
					foreach ($oldAdministraciones as $oldAdministracione) {
						if ($oldAdministracione->porcentaje == $request->porcentaje) { //de existir una administracion igual, retornamos un error al cliente
							return response()->json([
								KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
								KeysResponse::KEY_MESSAGE => 'Ya exite este porcentaje de administracion para este tipo de tarjeta',
								KeysResponse::KEY_DATA    => null,
							], CodesResponse::CODE_BAD_REQUEST);
						}
					}
				}
				if ($codigo == Tarjetas::CODIGO_SERVICIO_REGALO) {// si el servicio es regalo busco se busca si hay una parametrizacion activa
					$existePararegalo = AdminisTarjetas::query()
													   ->where('estado', AdminisTarjetas::ESTADO_ACTIVO)
													   ->where('servicio_codigo', $codigo)
													   ->first();
					if ($existePararegalo != null) {// de existir retorno un error al cliente
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => 'Existe una parametrizacion activa para este producto',
							KeysResponse::KEY_DATA    => null,
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				$parametro                  =
					new AdminisTarjetas($request->all());//creamos una instancia del modelo Administracion y lo inicializamos con el request
				$parametro->servicio_codigo = $codigo; // definimos el servicio
				$parametro->save(); // guardamos los cambios

				// retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $parametro,
				], CodesResponse::CODE_CREATED);

			} catch (\Exception $exception) {
				//de ocurrir un error en la operacion le retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * metodo encargado de llenar la grid con las configuraciones de administracion existentes para una tarjeta
		 *
		 * @return mixed
		 */
		public function gridAdministracionTarjetas($codigo) {

			//buscamos las administracion existente para el servicio
			$administraciones = AdminisTarjetas::with("getTipoTarjeta")
											   ->where('estado', AdminisTarjetas::ESTADO_ACTIVO)
											   ->where('servicio_codigo', $codigo)
											   ->orderBy("porcentaje", "asc");

			//retornamos los la data de una forma que el datatable de la vista pueda mostrarla
			return Datatables::of($administraciones)
							 ->addColumn('action', function ($rangos) {
								 $acciones = '<div class="btn-group">';
								 $acciones = $acciones.'<button class="btn btn-xs btn-danger" onclick="eliminarAdministracion('.$rangos->id
									 .')" ><i class="fa fa-trash"></i> Eliminar</button>';
								 $acciones = $acciones.'</div>';

								 return $acciones;
							 })->make(true);

		}

		/**
		 * metodo que permite eliminar(cambiar de estado) una parametricacion de porcentaje de administracion
		 *
		 * @param Request $request
		 */
		public function tarjetaEliminarParametroAdministracion(Request $request) {
			try {
				$administracion         = AdminisTarjetas::query()->findOrFail($request->id); //consultamos la administracion a cambiar de estado
				$administracion->estado = AdminisTarjetas::ESTADO_INACTIVO; //cambiamos de estado a estado inactivo la administracion
				$administracion->save();// guardamos los cambios

				// retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_DELETE_SUCCESS,
					KeysResponse::KEY_DATA    => $administracion,
				], CodesResponse::CODE_OK);
			} catch (\Exception $exception) {
				// de ocurrir un error retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}


		/**
		 * metodo encargado de tarer la vista parcial de parametrizacion de tarjetas
		 *
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function getViewParametrizarServicio(Request $request) {
			$servicio      = Servicios::where('codigo', $request->codigo)->first(); //optenemos el detalle del servicio a parametrizar
			$departamentos = Departamentos::all()->pluck('descripcion', 'codigo'); // traemos la lista de departamentos para mostrar en la vista
			if ($servicio->tipo == 'P') { // si el tipo de servicio es producto, mostramo la vista de para metrizacion servicio producto
				return view('tarjetas.parametrizacion.partialparametrizacionproducto', compact('servicio', 'departamentos'));
			}
		}

		/**
		 * metodo que me carga el historial del cambio del valor del plastico
		 *
		 * @return mixed
		 */
		public function gridValorPlastico() {
			$valorPlasticos = ValorTarjeta::all(); // traemos todos los valores configurados para el costo del plastico
			//retornamos a la vista los valores configurados para el costo del plastico, en un formato que entiede el datatable
			return Datatables::of($valorPlasticos)->make(true);
		}

		/**
		 * Permite definir si el servicio paga o no el valor del plastico
		 *
		 * @param \Illuminate\Http\Request $request
		 * @param                          $codigo
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function tarjetaCrearParametroPagaplastico(Request $request, $codigo) {
			DB::beginTransaction();// iniciamos una transacion en la base de datos, para evitar datos huerfanos en caso de error
			try {
				//se consulta, que si para este servicio, hay una configuracion de paga plastico
				$existePaga = PagaPlastico::where('estado', PagaPlastico::ACTIVA)->where('servicio_codigo', $codigo)->first();

				if ($existePaga != null) { // de existir una configuracion de paga platico, se inactiva (solo puede haber una configuracion activa por servicio)
					$existePaga->estado = PagaPlastico::INACTIVA;
					$existePaga->save();
				}
				$newPagaPlastico = new PagaPlastico();// instanciamos un modelo vacio de PagaPlastico

				/* inicializamos los actributos del objeto */
				$newPagaPlastico->pagaplastico    = $request->pagaplatico;
				$newPagaPlastico->estado          = PagaPlastico::ACTIVA;
				$newPagaPlastico->servicio_codigo = $codigo;
				$newPagaPlastico->save();// guardamos los cambios del objeto
				DB::commit();// guardamos los cambios en la base de datos

				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $newPagaPlastico,
				], CodesResponse::CODE_CREATED);
			} catch (\Exception $exception) {
				DB::rollBack();// de ocurrir un cambio descartamos los cambios en la base de datos

				//retornamos un el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_CREATED);
			}
		}

		/**
		 * metodo que alimenta la parametrica para saber si un servicio paga o no plastico
		 *
		 * @param $codigo el codigo del servicio
		 *
		 * @return Datatables
		 */
		public function gridServicioPagaPlastico($codigo) {
			$pagaPlastico = PagaPlastico::where('servicio_codigo', $codigo)->get(); // buscamos el historico de paga plastico para el servicio

			return Datatables::of($pagaPlastico)->make(true); //lo retornamos a al datable en un formato esperado
		}

		/**
		 * metodo que agrega una cuenta contable a el servicio de bono y tarjeta regalo
		 *
		 * @param Request $request
		 * @param         $codigo el codigo del servicio
		 *
		 * @return array
		 */
		public function tarjetaCrearParametroCuentaRB(Request $request, $codigo) {
			DB::beginTransaction();//inicializamos una transacion en la base datos para evitar datos huerfanos en caso de error
			try {
				if ($codigo != Servicios::CODIGO_SERVICIO_AFILIADO) { //si el servicio es diferente a afiliado(CupoRotativo) Inactivamos la cuenta contable anterior
					CuenContaTarjeta::query()->where('estado', CuenContaTarjeta::ACTIVA)
									->where('servicio_codigo', $codigo)
									->update(['estado' => CuenContaTarjeta::INACTIVA]);
				}
				else {//si el servicio es cupo rotativo, se tiene en cuenta tambien el municipio para inactivar la anterior
					CuenContaTarjeta::where('estado', CuenContaTarjeta::ACTIVA)
													->where('servicio_codigo', $codigo)
													->where('municipio_codigo', $request->municipio_codigo)
													->update(['estado' => CuenContaTarjeta::INACTIVA]);
				}
				$newCutenta                   = new CuenContaTarjeta(); //instanaciamos un nuevo objeto de la clase CuenContaTarjeta
				/* inicilizamos los atribustos del objeto */
				$newCutenta->estado           = CuenContaTarjeta::ACTIVA;
				$newCutenta->servicio_codigo  = $codigo;
				$newCutenta->cuenta           = $request->cuentacontable;
				$newCutenta->municipio_codigo = $codigo != Servicios::CODIGO_SERVICIO_AFILIADO ? '50001' : $request->municipio_codigo;
				$newCutenta->save(); //guardamos los cambios del objeto
				DB::commit(); // guardamos en base de datos

				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $newCutenta,
				], CodesResponse::CODE_CREATED);
			} catch (\Exception $exception) {
				DB::rollBack(); //si ocurre un error descartamos los cambios de la base de datos

				//retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * metodo que me carga la grid, que me muestra las cuentas contables que tenga un servicio
		 *
		 * @param $codigo el codigo del servicio
		 *
		 * @return mixed
		 */
		public function gridParametrosCuentasContables($codigo) {
			$cuentas = CuenContaTarjeta::query()->where('servicio_codigo', $codigo)->get();
			foreach ($cuentas as $cuenta) {
				$cuenta->getMunicipio;
			}

			return Datatables::of($cuentas)->make(true);
		}


		public function viewTipoTarjetas(Request $request) {

			if ($request->ajax()) {

				$query = TipoTarjeta::with('servicios');


				return Datatables::eloquent($query)
								 ->addColumn('descripcion', function (TipoTarjeta $tipoTarjeta) {
									 return $tipoTarjeta->servicios->map(function ($servicios) {
										 return $servicios->descripcion;
									 })->implode(",  ");
								 })
								 ->addColumn('action', function ($tipoTarjeta) {
									 $acciones = '<div class="btn-group">';

									 $acciones = $acciones.'<a href="'.route("editarTipoTarjetas", ["id" => $tipoTarjeta->id])
										 .'" data-modal="modal" class="btn btn-xs btn-info"><i class="ti-pencil-alt"></i> Editar</a>';

									 if ($tipoTarjeta->estado == TipoTarjeta::ESTADO_ACTIVO) {
										 $acciones = $acciones.'<button class="btn btn-xs btn-success" onclick="cambiarEstadoTipoTarjeta('.$tipoTarjeta->id
											 .')" ><i class="mdi mdi-checkbox-marked-circle"></i> Inactivar</button>';
									 }
									 else {
										 $acciones = $acciones.'<button class="btn btn-xs btn-warning" onclick="cambiarEstadoTipoTarjeta('.$tipoTarjeta->id
											 .')" ><i class="mdi mdi-checkbox-marked-circle"></i> Activar</button>';
									 }

									 $acciones = $acciones.'</div>';

									 return $acciones;
								 })->make(true);

			}


			$data["servicios"] = Servicios::all();

			return view('tarjetas.tipotarjetas', $data);

		}


		public function crearTipoTarjetas(Request $request) {
			$result = [];
			DB::beginTransaction();
			try {

				$tipoTarjeta = TipoTarjeta::create($request->all());

				$tipoTarjeta->servicios()->sync($request->servicios);

				$result['estado']  = true;
				$result['mensaje'] = 'Se creo el tipo de tarjeta satisfactoriamente';
				DB::commit();
			} catch (\Exception $exception) {
				DB::rollBack();
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible crear el tipo de tarjeta. '.$exception->getMessage();
			}

			return $result;

		}

		/**
		 *
		 */
		public function viewEditarTipoTarjetas(Request $request) {

			$tipoTarjeta = TipoTarjeta::query()->find($request->id);

			$servicios = Servicios::all();


			return view('tarjetas.modaledittipotarjeta', compact("tipoTarjeta", "servicios"));


		}

		public function editarTipoTarjetas(Request $request, $id) {

			//			dd($request->all());

			$result = [];
			DB::beginTransaction();
			try {

				$tipoTarjeta         = TipoTarjeta::query()->find($id);
				$tipoTarjeta->nombre = $request->nombre;
				$tipoTarjeta->save();

				$tipoTarjeta->servicios()->sync($request->servicios);

				$result['estado']  = true;
				$result['mensaje'] = 'Se Edito el tipo de tarjeta satisfactoriamente';
				DB::commit();
			} catch (\Exception $exception) {
				DB::rollBack();
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible crear el tipo de tarjeta. '.$exception->getMessage();
			}

			return $result;

		}


		/**
		 * metodo que cambia el estado de una terminal especifica
		 *
		 * @param Request $request trae la id de la terminal que se desea cambiar de estado
		 *
		 * @return array
		 */
		public function cambiarEstadoTipoTarjeta(Request $request) {
			$result = [];
			try {
				$tipoTarjeta = TipoTarjeta::find($request->id);

				if ($tipoTarjeta->estado == 'A') {
					$tipoTarjeta->estado = 'I';
				}
				else {
					$tipoTarjeta->estado = 'A';
				}
				$tipoTarjeta->save();
				$result['estado']  = true;
				$result['mensaje'] = 'Se cambio de estado satisfactoriamente';

			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible cambiar el estado '.$exception->getMessage();
			}

			return $result;

		}

		/**
		 * Metodo que permite definir el cupo minimo pertido para las tarjetas regalo y bono empresarial
		 *
		 * @param \Illuminate\Http\Request $request
		 * @param                          $codigo
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function crearCupoMinimo(Request $request, $codigo) {

			DB::beginTransaction(); //iniciamos una transacion en la base de datos
			try {
				//consultamos el utimo cupominimo parametrizado para este servicio
				$anteriorCupoMinimo = CupoMinimo::query()->where('servicio_codigo', $codigo)
												->orderBy('created_at', 'desc')
												->first();

				if ($anteriorCupoMinimo != null) {// si hay un valor parametrizado lo cambiamos de estado y guardamos
					$anteriorCupoMinimo->estado = CupoMinimo::INACTIVO;
					$anteriorCupoMinimo->save();
				}

				$cupoMinimo                  = new CupoMinimo(); // instanaciamos un cupominimo vacio
				$cupoMinimo->estado          = CupoMinimo::ACTIVO; // asinamos el nuevo cupo enestado activo
				$cupoMinimo->servicio_codigo = $codigo; // difinimos el servicio al que se va asignar la parametrizacion
				// definimos el cupo minimo que resivimos del request
				$cupoMinimo->cupominimo = str_replace(",", ".", str_replace(".", "", $request->cupominimo));
				$cupoMinimo->save(); // guardamos los cambios del modelo
				DB::commit(); // guardamos los cambios en la base de datos

				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $cupoMinimo,
				], CodesResponse::CODE_CREATED);
			} catch (\Exception $exception) {
				DB::rollBack();// si ocurre un error descartamos los cambios en la base de datos
				//retornamos una respuesta de error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_ERROR,
					KeysResponse::KEY_DATA    => $exception->getMessage(),
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * retornamos los datos a la grid que muestra el historico de cupo minimo para el servicio
		 * @param $codigo
		 *
		 * @return mixed
		 */
		public function gridCupoMinimo($codigo) {
			//consultamos los cuposminimos que se han creados para el servicio
			$cupominimos = CupoMinimo::query()
									 ->where('servicio_codigo', $codigo)
									 ->orderBy("created_at", "desc");
			//lo retornamos a al datatable que muestra el historico de cupos minimo
			return Datatables::of($cupominimos)->make(true);
		}

	}
