<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class DetalleProdutos extends Model implements AuditableContract
{
    use Auditable;
	const ESTADO_INACTIVO = "I";
    const ESTADO_ACTIVO = "A";
    const ESTADO_ANULADO = "N";
    const ESTADO_CONSUMIDO = 'C';

    protected $fillable = [
        'numero_tarjeta',
        'fecha_cracion',
        'monto_inicial',
        'contrato_emprs_id',
        'user_id',
        'estado',
        'fecha_activacion',
        'fecha_vencimiento',
        'factura',
    ];

    public function getEstadoAttribute($value)
    {
        $result = "";
        switch ($value) {
            case "A":
                $result = "Activo";
                break;
            case "I":
                $result = "Inactiva";
                break;
			case "C":
				$result = "Consumido";
				break;
            default:
                $result = "Anulado";

        }

        return $result;
    }

	/**
	 * relacion de uno a uno con entre el detalle producto y el usuario
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function getUser()
    {
        return $this->belongsTo('creditocofrem\User', 'user_id', 'id');
    }

	/**
	 * relacion logica de uno a uno entre el detalle producto y la tarjeta
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function tarjeta()
    {
        return $this->belongsTo(Tarjetas::class, "numero_tarjeta", "numero_tarjeta");
    }

	/**
	 * relacion uno a uno entre el detalle producto y el cotrato
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function contrato(){
		return $this->belongsTo(Contratos_empr::class, "contrato_emprs_id", "id");
	}

	public function transaciones(){
    	return $this->belongsToMany(
    		Transaccion::class,'detalle_transacciones',
			'detalle_producto_id',
			'transaccion_id'
		)->withPivot(['valor','descripcion']);
	}




}
