<?php

	namespace creditocofrem;

	use Illuminate\Database\Eloquent\Model;
	use OwenIt\Auditing\Auditable;
	use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

	class Empresas extends Model implements AuditableContract {

		//
		use Auditable; // se define que esta table sera auditada

		// se crean costantes globales para los tipos de empresa
		const AFILIADO = 'A';
		const TERCERO = 'T';

		// se define las propiedasdes que seran acedidas de manera masiva
		protected $fillable = [
			'nit', 'razon_social', 'representante_legal', 'municipio_codigo', 'email', 'telefono', 'celular', 'direccion', 'tipo', 'tipo_documento',
		];

		//se crea relacion logica entre empresas y municipios
		public function getMunicipio() {
			return $this->belongsTo(Municipios::class, 'municipio_codigo', 'codigo');
		}

		// TODO esta relacion, analizar si realmente se susa (a mi cirterio no se esta usando y ni siquiera es valida)
		public function getEstablecimiento() {
			return $this->belongsTo('creditocofrem\Establecimientos', 'establecimiento_id', 'id');
		}

		// se crea relacion logica entre empresa y los tipos de documentos
		public function getTipoDocumento() {
			return $this->belongsTo('creditocofrem\TipoDocumento', 'tipo_documento', 'tip_codi');
		}


	}
