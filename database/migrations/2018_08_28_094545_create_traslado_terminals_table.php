<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrasladoTerminalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traslado_terminals', function (Blueprint $table) {
            $table->increments('id')->nocache();
            $table->unsignedBigInteger('terminal_id');
            $table->unsignedBigInteger('origen_id');
            $table->unsignedBigInteger('destino_id');
			$table->foreign('origen_id')->references('id')->on('sucursales')->onDelete('cascade');
			$table->foreign('destino_id')->references('id')->on('sucursales')->onDelete('cascade');
			$table->foreign('terminal_id')->references('id')->on('terminales')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traslado_terminals');
    }
}
