<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturaRegalosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_regalos', function (Blueprint $table) {
            $table->increments('id')->nocache();
			$table->string("tip_docu");
			$table->string("cli_coda");
			$table->string("cli_noco");
			$table->string("top_codi");
			$table->string("fac_cont");
			$table->string("tip_codi");
			$table->string("tip_nomb");
			$table->string("cxc_nume");
			$table->string("cxc_fech");
			$table->string("cxc_cont");
			$table->string("cxc_tota");
			$table->string("cxc_pago");
			$table->string("cxc_sald");
			$table->string("cxc_feve");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_regalos');
    }
}
