<?php

use Illuminate\Database\Seeder;

class MotivoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('motivos')->insert([
                ["id" => "1", "codigo" => '01', "motivo" => 'CREACIÓN', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "2", "codigo" => '02', "motivo" => 'INICIALIZACIÓN DE SERVICIO', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "3", "codigo" => '03', "motivo" => 'ACTIVACIÓN DE SERVICIOS', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "4", "codigo" => '04', "motivo" => 'INACTIVA POR CONSUMO DE SERVICIOS', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "5", "codigo" => '05', "motivo" => 'INACTIVA SIN SERVICIOS', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "6", "codigo" => '06', "motivo" => 'INICIALIZACIÓN POR DUPLICADO', 'tipo' => 'T', 'estado' => 'A'],

                ["id" => "7", "codigo" => '07', "motivo" => 'ASIGNACION DEL SERVICIO', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "8", "codigo" => '08', "motivo" => 'ACTIVACIÓN DEL SERVICIO', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "9", "codigo" => '09', "motivo" => 'SERVICIO CONSUMIDO', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "10", "codigo" => '10', "motivo" => 'VENCIMIENTO DE SERVICIOS', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "11", "codigo" => '11', "motivo" => 'SERVICIO INACTIVO POR INCUMPLIMIENTO DE PLAGO', 'tipo' => 'T', 'estado' => 'A'],
                ["id" => "12", "codigo" => '12', "motivo" => 'INACTIVACIÓN POR FINALIZACIÓN DE CONVENIO', 'tipo' => 'T', 'estado' => 'A'],

                ["id" => "13", "codigo" => '13', "motivo" => 'DUPLICADO POR PERDIDA', 'tipo' => 'D', 'estado' => 'A'],
                ["id" => "14", "codigo" => '14', "motivo" => 'DUPLICADO POR DETERIORO', 'tipo' => 'D', 'estado' => 'A'],
                ["id" => "15", "codigo" => '15', "motivo" => 'DUPLICADO POR ROBO', 'tipo' => 'D', 'estado' => 'A']
            ]
        );
    }
}
