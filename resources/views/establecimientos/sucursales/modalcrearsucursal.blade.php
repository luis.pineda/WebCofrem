<div id="modalcrearsucursal" class="principal">
    {{Form::open(['route'=>['sucursal.crearp', $establecimiento_id], 'class'=>'form-horizontal', 'id'=>'crearsucursal'])}}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Agregar sucursal</h4>
    </div>
    <div class="modal-body">

        <div class="form-group">
            <label class="col-md-2 control-label">Nombre</label>
            <div class="col-md-10">
                {{Form::text('nombre', null ,['class'=>'form-control toupercase', "id"=>"nombre", "required", "tabindex"=>"1","maxlength"=>"60"])}}

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-12">Ubicacion</label>
            <div class="col-md-12">
                <div id="map2" style="width: 100%;height: 250px"></div>
            </div>
            {{Form::hidden('latitud',null,['id'=>'latitud'])}}
            {{Form::hidden('longitud',null,['id'=>'longitud'])}}
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Departamento</label>
            <div class="col-md-4">
                {{Form::select("departamento_codigo",$departamentos,null,['class'=>'form-control', "tabindex"=>"2",'id'=>'departamento', 'placeholder'=>'Seleccione...'])}}
            </div>
            <label class="col-md-2 control-label">Ciudad</label>
            <div class="col-md-4">
                <select name="municipio_codigo" id="municipio" tabindex="3" class="form-control">
                    <option>Seleccione...</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Dirección</label>
            <div class="col-md-8">
                <input type="text" id="direccion" name="direccion" tabindex="8" class="form-control obtener" readonly>
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary form-control" type="button" disabled id="gdireccion" onclick="generardireccion()">Generar</button>
            </div>

        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Contacto</label>
            <div class="col-md-10">
                {{Form::text('contacto', null ,['class'=>'form-control toupercase',  "required", "tabindex"=>"9","maxlength"=>"60", "data-parsley-pattern"=>"^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ]+(\s*[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ]*)*[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ]+$"])}}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Email</label>
            <div class="col-md-10">
                {{Form::text('email', null ,['class'=>'form-control',  "required", "tabindex"=>"9","maxlength"=>"60", "data-parsley-type"=>"email"])}}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Telefono</label>
            <div class="col-md-10">
                {{Form::text('telefono', null ,['class'=>'form-control',  "required", "tabindex"=>"10","maxlength"=>"10", "data-parsley-type"=>"number"])}}

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Contraseña</label>
            <div class="col-md-10">
                {{Form::password('password', ['class'=>'form-control', "required", "data-parsley-type"=>"number", "tabindex"=>"11","maxlength"=>"4"])}}
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-custom waves-effect waves-light">Guardar</button>
    </div>
    {{Form::close()}}
</div>


<div class="tipozona pre" style="display: none">
    <div class="modal-header">
        <button type="button" class="close pasos" onclick="cerrarpasos()">×</button>
        <h4 class="modal-title">Seleccione el tipo de Zona</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <label class="control-label col-md-4">Zona</label>
            <div class="col-md-8">
                <select class="form-control" id="zona" name="zona" onchange="cambioTipo()">
                    <option selected="selected" value="">---Seleccione---</option>
                    <option value="U">Urbana</option>
                    <option value="R">Rural</option>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="rural pre" style="display: none">
    <div class="modal-header">
        <button type="button" class="close pasos" onclick="cerrarpasos()">×</button>
        <h4 class="modal-title">Asistente Para el ingreso de la Dirección</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-5">
                <select class="form-control"
                        id="datoAdicionalRural" name="datoAdicional">
                    <option selected="selected" value="">---Seleccione via---</option>
                    <option value="CN">Camino</option>
                    <option value="CT ">Carretera</option>
                    <option value="CA">Casa</option>
                    <option value="EN">Entrada</option>
                    <option value="FINC">Finca</option>
                    <option value="KM">Kilómetro</option>
                    <option value="LT">Lote</option>
                    <option value="MZ">Manzana</option>
                    <option value="PS">Paseo</option>
                    <option value="PN">Puente</option>
                    <option value="PD">Predio</option>
                    <option value="SR">Sector</option>
                    <option value="SL">Solar</option>
                    <option value="VDA">Vereda</option>
                    <option value="ZN">Zona</option>
                </select>
            </div>
            <div class="col-md-5">
                <input type="text" name="complementorural" id="complementorural" class="form-control"
                       placeholder="Complemento">
            </div>
            <div class="col-md-2">
                <button type="button" class="form-control btn btn-primary" onclick="guardarRural()">Guardar</button>
            </div>
        </div>
    </div>
</div>


<div class="urbana pre" style="display: none">
    <div class="modal-header">
        <button type="button" class="close pasos" onclick="cerrarpasos()">×</button>
        <h4 class="modal-title">Asistente Para el ingreso de la Dirección</h4>
    </div>
    <div class="modal-body">

        <div class="row">

            <div class="form-group">
                <div class="col-md-4">
                    <select class="form-control"
                            id="via" name="via">
                        <option value="">Seleccione via principal</option>
                        <option value="AP">AUTOPISTA</option>
                        <option value="AV">AVENIDA</option>
                        <option value="AC">AVENIDA CALLE</option>
                        <option value="AK">AVENIDA CARRERA</option>
                        <option value="CL" selected="selected">CALLE</option>
                        <option value="CRA">CARRERA</option>
                        <option value="CIRC">CIRCUNVALAR</option>
                        <option value="DG">DIAGONAL</option>
                        <option value="MANZ">MANZANA</option>
                        <option value="TV">TRANSVERSAL</option>
                        <option value="VIA">VIA</option>
                    </select>
                </div>
                <div class="col-md-3">

                    <input class="form-control" id="numero" placeholder="Numero..." maxlength="3" name="numero"
                           type="number">

                </div>

                <div class="col-md-1">
                    <select class="form-control" id="letra"
                            name="letra">
                        <option value="">Letra</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="F">F</option>
                        <option value="G">G</option>
                        <option value="H">H</option>
                        <option value="I">I</option>
                        <option value="J">J</option>
                        <option value="K">K</option>
                        <option value="L">L</option>
                        <option value="M">M</option>
                        <option value="N">N</option>
                        <option value="O">O</option>
                        <option value="P">P</option>
                        <option value="Q">Q</option>
                        <option value="R">R</option>
                        <option value="S">S</option>
                        <option value="T">T</option>
                        <option value="U">U</option>
                        <option value="V">V</option>
                        <option value="W">W</option>
                        <option value="X">X</option>
                        <option value="Y">Y</option>
                        <option value="Z">Z</option>
                    </select>
                </div>

                <div class="col-md-1">
                    <input id="bis" name="bis" type="checkbox" value="BIS"><label for="bis">BIS</label>
                </div>

                <div class="col-md-1">
                    <select class="form-control" id="letraa" name="letraa">
                        <option value="">Letra</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="F">F</option>
                        <option value="G">G</option>
                        <option value="H">H</option>
                        <option value="I">I</option>
                        <option value="J">J</option>
                        <option value="K">K</option>
                        <option value="L">L</option>
                        <option value="M">M</option>
                        <option value="N">N</option>
                        <option value="O">O</option>
                        <option value="P">P</option>
                        <option value="Q">Q</option>
                        <option value="R">R</option>
                        <option value="S">S</option>
                        <option value="T">T</option>
                        <option value="U">U</option>
                        <option value="V">V</option>
                        <option value="W">W</option>
                        <option value="X">X</option>
                        <option value="Y">Y</option>
                        <option value="Z">Z</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select class="form-control" id="direcci"
                            name="direcci">
                        <option value=""></option>
                        <option value="Norte">NORTE</option>
                        <option value="Oeste">OESTE</option>
                        <option value="Este">ESTE</option>
                        <option value="Sur">SUR</option>
                    </select>
                </div>
            </div>

            <div class="col-md-12">
                &nbsp;
            </div>

            <div class="form-group">
                <div class="col-md-1 text-right">
                    #
                </div>
                <div class="col-md-3">
                    <input class="form-control" placeholder="Numero..." id="numero1" maxlength="3" name="numero1"
                           type="number">
                </div>
                <div class="col-md-1">
                    <select class="form-control" id="letrab" name="letrab">
                        <option value="">Letra</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="F">F</option>
                        <option value="G">G</option>
                        <option value="H">H</option>
                        <option value="I">I</option>
                        <option value="J">J</option>
                        <option value="K">K</option>
                        <option value="L">L</option>
                        <option value="M">M</option>
                        <option value="N">N</option>
                        <option value="O">O</option>
                        <option value="P">P</option>
                        <option value="Q">Q</option>
                        <option value="R">R</option>
                        <option value="S">S</option>
                        <option value="T">T</option>
                        <option value="U">U</option>
                        <option value="V">V</option>
                        <option value="W">W</option>
                        <option value="X">X</option>
                        <option value="Y">Y</option>
                        <option value="Z">Z</option>
                    </select>
                </div>

                <div class="col-md-1">
                    -
                </div>

                <div class="col-md-3">
                    <input class="form-control" placeholder="Numero..." id="numero2" maxlength="3" name="numero2"
                           type="number">

                </div>

                <div class="col-md-2">
                    <select class="form-control" id="direccion1" name="direccion1">
                        <option value=""></option>
                        <option value="Norte">NORTE</option>
                        <option value="Oeste">OESTE</option>
                        <option value="Este">ESTE</option>
                        <option value="Sur">SUR</option>
                    </select>
                </div>

            </div>

            <div class="col-md-12">
                &nbsp;
            </div>
            <div class="col-md-12 text-center">
                <button type="button" class="btn btn-primary" onclick="guardarUrbana()">Guardar</button>
            </div>
        </div>

    </div>
</div>

<script>
    $(function () {
        setTimeout(function () {
            $('#nombre').focus();
        }, 1000);
        $("#crearsucursal").parsley();
        $("#crearsucursal").submit(function (e) {
            e.preventDefault();
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    cargando();
                },
                success: function (result) {
                    if (result.status) {
                        swal(
                            {
                                title: 'Bien!!',
                                text: result.message,
                                type: 'success',
                                confirmButtonColor: '#4fa7f3'
                            }
                        );
                    }
                    addMarket();
                    table.ajax.reload();
                    modalBs.modal('hide');
                },
                error: function (xhr, status) {
                    var mensajefinal;
                    var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                    mensajefinal = message + '\n';
                    mensajefinal += xhr.responseJSON.message + '\n';
                    if (xhr.status === 422) {
                        var errores = "";
                        for (i = 0; i < xhr.responseJSON.data.length; i++) {
                            errores += xhr.responseJSON.data[i] + '\n';
                        }
                        mensajefinal += errores;
                        swal({
                            type: 'error',
                            title: 'Error...',
                            text: mensajefinal,
                        })
                    } else {
                        swal(
                            'Error!!',
                            mensajefinal
                            ,
                            'error'
                        )
                    }
                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    fincarga();
                }
            });
        });

        initMap2();
        //setTimeout(getMunicipios, '300');
        $("#departamento").change(function () {
            getMunicipios();
        });

        $('.obtener').blur(function () {
            geolocalizarDireccion();
        });


    });

    var map2;

    function initMap2() {
        setTimeout(function () {
            map2 = new GMaps({
                div: '#map2',
                lat: 4.1405896,
                lng: -73.6369522
            });
        }, 200);
    };


    function geolocalizarDireccion2() {
        GMaps.geocode({
            address: $('#direccion').val() + ' '+ $("#municipio option:selected").html(),
            callback: function (results, status) {
                if (status == 'OK') {
                    var latlng = results[0].geometry.location;
                    map2.setCenter(latlng.lat(), latlng.lng());
                    map2.removeMarkers();
                    map2.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng()
                    });
                    $('#latitud').val(latlng.lat());
                    $('#longitud').val(latlng.lng());
                }
            }
        });
    }

    function addMarket() {
        var latitud = $('#latitud').val();
        var longitud = $('#longitud').val();
        var nombre = $('#nombre').val();
        map.setCenter(latitud, longitud);
        map.addMarker({
            lat: latitud,
            lng: longitud,
            title: nombre,
            infoWindow: {
                content: '<p>' + nombre + '</p>'
            }
        });
    }

    function getMunicipios() {
        var dept = $("#departamento").val();
        if(dept !== ""){
            $.get('{{route('municipios')}}', {data: dept}, function (result) {
                $('#municipio').html("");
                $.each(result, function (i, value) {
                    $('#municipio').append($('<option>').text(value.descripcion).attr('value', value.codigo));
                });
                $('#gdireccion').attr('disabled', false);
            })
        }else{
            $('#gdireccion').attr('disabled', true);
            $('#municipio').html("");
            $('#municipio').append($('<option>').text("Seleccione...").attr('value', ""));
        }
    }

    function generardireccion() {
        $('.principal').hide();
        $('.tipozona').show();
    }


    function cambioTipo() {
        var zona = $('#zona').val();
        if (zona !== "") {
            $('.tipozona').hide();
            if (zona === 'R') {
                $('.rural').show();
            } else {
                $('.urbana').show();
            }
        }
        $('#zona').val("");
    }

    function cerrarpasos() {
        $('.pre').hide();
        $('.principal').show();
    }

    function guardarRural() {
        var via = $("#datoAdicionalRural").val();
        var complementorural = $('#complementorural').val();
        if (via !== "" && complementorural !== "") {
            $('#direccion').val(via + ' ' + complementorural);
            cerrarpasos();
            geolocalizarDireccion2();
        }
    }


    function guardarUrbana() {
        var via = $('#via').val();
        var numero = $('#numero').val();
        var letra = $('#letra').val();
        var bis = $('#bis').val();
        var letraa = $('#letraa').val();
        var direcci = $('#direcci').val();
        var numero1 = $('#numero1').val();
        var letrab = $('#letrab').val();
        var numero2 = $('#numero2').val();
        var direccion1 = $('#direccion1').val();

        var final = via + ' ' + numero +  letra + ' ';


        if ($('#bis').is(':checked')) {
            final +=  bis + '';
        }
        if(letraa !==""){
            final += letraa + ' ';
        }
        if(direcci !== ""){
            final += direcci + ' #'
        }else{
           final += '#';
        }
        final +=  numero1 +letrab + ' - ' + numero2 + ' ' + direccion1

        $('#direccion').val(final);
        cerrarpasos();
        geolocalizarDireccion2();

    }

</script>