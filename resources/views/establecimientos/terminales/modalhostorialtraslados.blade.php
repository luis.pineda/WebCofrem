<div id="modaltrasladarterminal">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Historia traslado terminal codigo: {{$terminal->codigo}}</h4>
    </div>
    <div class="modal-body">

        <div class="table-responsive m-b-20">
            <table id="historial" class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Establecimiento origen</th>
                    <th>sucursal origen</th>
                    <th>Establecimiento destino</th>
                    <th>sucursal destino</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
    </div>
</div>

<script>
    var table3;
    $(function () {
        table3 = $('#historial').DataTable({
            processing: true,
            serverSide: true,
            "language": {
                "url": "{!!route('datatable_es')!!}"
            },
            ajax: {
                url: "{!!route('gridterminaleshistorial',[$terminal->id])!!}",
                "type": "get"
            },
            columns: [
                {data: 'created_at', name: 'created_at'},
                {data: 'origen.get_establecimiento.razon_social', name: 'origen.get_establecimiento.razon_social'},
                {data: 'origen.nombre', name: 'origen.nombre'},
                {data: 'destino.get_establecimiento.razon_social', name: 'destino.get_establecimiento.razon_social'},
                {data: 'destino.nombre', name: 'destino.nombre'},

            ],
        });
    });

</script>