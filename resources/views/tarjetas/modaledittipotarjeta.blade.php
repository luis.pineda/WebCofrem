<div id="modaleditartipotarjetas">
    {{Form::model($tipoTarjeta,['class'=>'form-horizontal editatar', 'id'=>'editartipotarjetas',"onsubmit"=> "funcion(this)"])}}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Editar Tarjeta</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <label class="col-md-2 control-label">Nombre</label>
                <div class="col-md-10">
                {{Form::text('nombre', null ,['class'=>'form-control toupercase',  "id"=>"nombre","tabindex"=>"1","required", "maxlength"=>"60", "data-parsley-type"=>"alpha"])}} <!-- "data-parsley-type"=>"number"] -->

                </div>
            </div>
            <br>
            <h5>Seleccione los servicios asociados a este tipo de tarjeta</h5>
            <div class="col-md-offset-2 col-md-10">

                @foreach($servicios as $servicio)

                    <div class="checkbox checkbox-custom">
                        <input id="checkbox{{$servicio->id}}" name="servicios[]" type="checkbox"
                               value="{{$servicio->id}}" {{($tipoTarjeta->servicios->contains($servicio->id)?"checked":"")}}>
                        <label for="checkbox{{$servicio->id}}">
                            {{$servicio->descripcion}}
                        </label>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-custom waves-effect waves-light" >Guardar</button>
    </div>
    {{Form::close()}}
</div>

<script>
    $(function () {

        $("#editartipotarjetas").parsley();

        $("#modaleditartipotarjetas").on('submit','#editartipotarjetas', function (e) {
            e.preventDefault();
            var formEditarTipo = $("#editartipotarjetas");
            $.ajax({
                url: "{{route('editarTipoTarjetas',$tipoTarjeta->id)}}",
                data: formEditarTipo.serialize(),
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    cargando();
                },
                success: function (result) {
                    if (result.estado) {
                        swal(
                            {
                                title: 'Bien!!',
                                text: result.mensaje,
                                type: 'success',
                                confirmButtonColor: '#4fa7f3'
                            }
                        );
                        modalBs.modal('hide');
                    } else if (result.estado == false) {
                        swal(
                            'Error!!',
                            result.mensaje,
                            'error'
                        );
                    } else {
                        html = '';
                        for (i = 0; i < result.length; i++) {
                            html += result[i] + '\n\r';
                        }
                        swal(
                            'Error!!',
                            html,
                            'error'
                        )
                    }
                    table.ajax.reload();
                },
                error: function (xhr, status) {
                    var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                    swal(
                        'Error!!',
                        message,
                        'error'
                    )
                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    fincarga();
                }
            });
        })
    });

</script>