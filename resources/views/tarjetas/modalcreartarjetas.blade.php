<div id="modalcreartarjetas">
    {{Form::open(['route'=>['tarjetas.crearp'], 'class'=>'form-horizontal', 'id'=>'creartarjetas'])}}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Agregar Tarjeta</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <label class="col-md-4 control-label">Número de tarjeta</label>
                <div class="col-md-8">
                    {{Form::text('numero_tarjeta', null ,['class'=>'form-control', "required", "maxlength"=>"6", "data-parsley-type"=>"number", "tabindex"=>"1",'id'=>'numero_tarjeta'])}} <!-- "data-parsley-type"=>"number"] -->
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Tipo de tarjeta</label>
                <div class="col-md-8">
                    {{Form::select("tipo_tarjetas_id",$tipo_tarjetas,null,['class'=>'form-control ', "tabindex"=>"2",'id'=>'tipo_tarjetas_id', "required"])}}
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-custom waves-effect waves-light">Guardar</button>
    </div>
    {{Form::close()}}
</div>

<script>
    $(function () {
        $("#creartarjetas").parsley();
        $("#creartarjetas").submit(function (e) {
            e.preventDefault();
            var form = $(this);
            $.ajax({
                url : form.attr('action'),
                data : form.serialize(),
                type : 'POST',
                dataType : 'json',
                beforeSend: function () {
                    cargando();
                },
                success: function (result) {
                    responseSuccess('Bien!!', result.message);
                    modalBs.modal('hide');
                    table.ajax.reload();
                },
                error: function (xhr, status) {
                    responseError(xhr);
                },
                complete: function (xhr, status) {
                    fincarga();
                }
            });
        });


        $("#selectroles").select2({
            placeholder: "Seleccione...",
            minimumInputLength: 1,
            ajax: {
                url: "{{route('selectroles')}}",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    return {
                        term: params.term
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
            },
            language: "es",
            cache: true
        });
    })

</script>