<div id="modalduplicartarjetas">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Historial tarjeta {{$tarjeta->numero_tarjeta}}</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive m-b-20">
            <table id="historialtarjeta" class="table table-striped table-bordered" width="100%">
                <thead>
                <tr>
                    <th>Fehca</th>
                    <th>Motivo</th>
                    <th>Estado Tarjeta</th>
                    <th>Servicio</th>
                    <th>Estado servicio</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
    </div>
</div>

<script>
    $(function () {
        $('#historialtarjeta').DataTable({
            processing: true,
            serverSide: true,
            "language": {
                "url": "{!!route('datatable_es')!!}"
            },
            ajax: {
                url: "{!!route('gridhistorialtarjeta',$tarjeta->id)!!}",
                "type": "get"
            },
            columns: [
                {data: 'fecha', name: 'fecha'},
                {data: 'motivo.motivo', name: 'motivo.motivo'},
                {data: 'estado', name: 'estado'},
                {data: 'servicio_codigo', name: 'servicio_codigo'},
                {data: 'estado_producto', name: 'estado_producto'},
            ]
        });
    });

</script>