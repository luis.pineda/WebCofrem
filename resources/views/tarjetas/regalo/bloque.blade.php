@extends('layouts.admin')

@section('styles')

@endsection

@section('contenido')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="header-title m-t-0 m-b-20">Asociar Tarjetas Regalo En Bloque </h4>
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box widget-inline">

                        <div class="row">
                            <form id="formCrearTajertaBloque" class="form-horizontal m-t-10" role="form">
                                <div style="margin-bottom: 15px;" class="row">
                                    <div class="col-sm-offset-2 col-sm-1">
                                        <select class="form-control" name="tip_docu" id="tip_docu">
                                            @foreach($tipoDocumentos as $tipo)
                                                <option data-descripcion="{{$tipo->descripcion}}" value="{{$tipo->id}}">{{$tipo->equivalente}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-7">
                                        {{Form::text('cli_coda', null ,['id'=>'nit','class'=>'form-control', "required","placeholder"=>"NIT", "maxlength"=>"15", "onkeypress"=>"return justNumbers(event)"])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="numero_factura" name="numero_factura" class="col-sm-3 control-label">Número
                                        Factura</label>
                                    <div class="col-sm-7">
                                        {{Form::text('numero_factura', null ,['class'=>'form-control', "required","placeholder"=>"Número de la factura", "maxlength"=>"15"])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="numero_tarjeta_inicial" class="col-sm-3 control-label">Número Tarjeta Inicial</label>
                                    <div class="col-sm-7">
                                    {{Form::text('numero_tarjeta_inicial', null ,['class'=>'form-control', "required","placeholder"=>"Número de la primera tarjeta", "maxlength"=>"6", "onkeypress"=>"return justNumbers(event)"])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cantidad" class="col-sm-3 control-label">Cantidad</label>
                                    <div class="col-sm-7">
                                        {{Form::text('cantidad', null ,['class'=>'form-control', "required","maxlength"=>"10","placeholder"=>"Cantidad","onkeypress"=>"return justNumbers(event)"])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="monto" class="col-sm-3 control-label">Monto</label>
                                    <div class="col-sm-7">
                                        {{Form::text('monto', null ,['class'=>'form-control money', "required", "maxlength"=>"10", "data-parsley-type"=>"number" ,"placeholder"=>"Monto"])}}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button id="btnCrearTRegalo" type="submit"
                                                class="btn btn-custom waves-effect waves-light">Asociar Servicio Regalo en Bloque
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('plugins/jQuery-Mask-Plugin/dist/jquery.mask.min.js')}}"></script>
    <script>
        $(function () {
            $('select').change(function(){

                $("#nit").attr("placeholder",$(this).find(':selected').data('descripcion'));
            });

            $('.money').mask('000.000.000.000.000', {reverse: true});

            $("#formCrearTajertaBloque").submit(function (e) {
                e.preventDefault();
                var form = $(this);

                $.ajax({
                    type: "POST",
                    context: document.body,
                    url: '{{route("addTarjetaRegaloBloque")}}',
                    data: form.serialize(),
                    beforeSend: function () {
                        cargando();
                    },
                    success: function (result) {
                        responseSuccess('Bien!!', result.message);
                        form.reset();
                    },
                    error: function (xhr, status) {
                        responseError(xhr);
                    },
                    complete: function (xhr, status) {
                        fincarga();
                    }
                });

            });

        });

    </script>
@endsection