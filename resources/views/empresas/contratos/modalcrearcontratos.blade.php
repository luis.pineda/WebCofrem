<div id="modalcrearcontratos">
    {{Form::open(['route'=>['contrato.crear'], 'files'=>'true' ,'class'=>'form-horizontal', 'id'=>'crearcontratos', 'target'=>"_blank", 'role'=>'form', 'method'=>'POST'])}}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Agregar contrato</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="form-group">
                <label class="col-md-4 control-label">Nit empresa</label>
                <div class="col-md-8">
                    {{Form::text('nit', null, ['class'=>'form-control', "required", "tabindex"=>"1", "maxlength"=>"10", "data-parsley-type"=>"number", 'id'=>'nit'])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Número de contrato</label>
                <div class="col-md-8">
                    {{Form::text('n_contrato', null ,['class'=>'form-control', "required", "tabindex"=>"2", 'id'=>'n_contrato','maxlength'=>'20'])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Valor contrato</label>
                <div class="col-md-8">
                    {{Form::text('valor_contrato', null ,['class'=>'form-control money', "required", "tabindex"=>"3",'id'=>'valor_contrato','maxlength'=>'20'])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Valor del impuesto</label>
                <div class="col-md-8">
                    {{Form::text('valor_impuesto', null ,['class'=>'form-control money', "required", "tabindex"=>"4",'id'=>'impuesto','maxlength'=>'20'])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Fecha de inicio</label>
                <div class="col-md-8">
                    {{Form::text("fecha", null,['class'=>'form-control', "required", "tabindex"=>"5", 'id'=>'fecha','readonly'])}}
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-4 control-label">Cantidad de tarjetas</label>
                <div class="col-md-8">
                    {{Form::text('n_tarjetas', null ,['class'=>'form-control', "required", "data-parsley-type"=>"number", "maxlength"=>"3", "tabindex"=>"6", 'id'=>'n_tarjetas'])}}
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-4 control-label">Forma de pago</label>
                <div class="col-md-8">
                    {{Form::select('forma_pago', ['E' => 'Efectivo', 'C' => 'Consumo'], null,['id'=>'forma_pago','class'=>'form-control', "tabindex"=>"7", "required", 'placeholder'=>'Seleccione la forma de pago'])}}
                </div>
            </div>


            <div id="divNumFactura" style="display: none">


                <div class="form-group" >
                    <label class="col-md-4 control-label">Número de Factura</label>
                    <div class="col-md-8">
                        {{Form::text('n_factura', null ,['id'=>'n_factura','class'=>'form-control', "required", "tabindex"=>"4",'maxlength'=>'30','disabled'])}}
                    </div>
                </div>

                <div class="form-group text-center">
                    <button id="validarFactura" type="button" class="btn btn-info waves-effect">Validar Factura</button>
                </div>

            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Documentos</label>
                <div class="col-md-8">
                    {{Form::file('pdf', ['class'=>'filestyle', "data-buttontext"=>"Buscar archivo", "tabindex"=>"8", 'id'=>'pdf'])}}
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-4 control-label">Porcentaje Administración</label>
                <div class="col-md-8">
                    {{Form::select("adminis_tarjeta_id",$administracion,null,['class'=>'form-control', "tabindex"=>"10", 'id'=>'administracion','required','placeholder'=>'Seleccione administración'])}}
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-custom waves-effect waves-light" id="btnCrearContrato">Guardar</button>
    </div>
    {{Form::close()}}
</div>


<script>


    $(function () {


        $("#forma_pago").change(function () {

            if ($(this).find(':selected').val() == "E") {
                $("#divNumFactura").show();
                $("#n_factura").removeAttr("disabled");
                $("#n_factura").attr("required", "required");
                $("#btnCrearContrato").attr("disabled", "disabled");
            } else {
                $("#divNumFactura").hide();
                $("#n_factura").attr("disabled", "disabled");
                $("#n_factura").removeAttr("required");
                $("#btnCrearContrato").removeAttr("disabled");
            }

        });

        $("#validarFactura").click(function () {
            $.ajax({
                type: "POST",
                context: document.body,
                url: '{{route("validarFactura")}}',
                data: {"nit":$("#nit").val(),"n_factura":$("#n_factura").val(),"valor_contrato":$("#valor_contrato").val()},
                beforeSend: function () {
                    cargando();
                },
                success: function (result) {
                    $("#btnCrearContrato").removeAttr("disabled");
                    responseSuccess('Bien!!',result.message);
                },
                error: function (xhr, status) {
                    $("#btnCrearContrato").attr("disabled", "disabled");
                    responseError(xhr);
                },
                complete: function (xhr, status) {
                    fincarga();
                }
            });
        });

        // funcion javascript para el autocompletar de nits
        $('#nit').autocomplete({
            serviceUrl: '{{route("autoCompleNitEmpresa")}}',
            //params:{"codigo":"R"},
            lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
                return re.test(suggestion.value);
            },
            onSelect: function (suggestion) {
                //console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
                $("#btnCrearContrato").removeClass("btn-custom").addClass("btn-success");
                $("#btnCrearContrato").removeAttr("disabled");
                $("#btnCrearContrato").html("Crear Contrato");
            },
            onHint: function (hint) {
                //$('#producto-x').val(hint);
            },
            onInvalidateSelection: function () {
                $("#btnCrearContrato").removeClass("btn-success").addClass("btn-custom");
                $("#btnCrearContrato").attr("disabled", "disabled");
            }
        });


        $('.money').mask('000.000.000.000.000', {reverse: true});
        $("#pdf").filestyle({
            buttonText: "Buscar archivo",
        });


        $('#fecha').datepicker({
            autoclose: true,
            startDate: moment().subtract(3, 'months').format('DD/MM/YYYY'),
            endDate: moment().add(3, 'months').format('DD/MM/YYYY'),
            format: 'dd/mm/yyyy',
            language: 'es',
            todayHighlight: true
        });

        $("#crearcontratos").parsley();
        $("#crearcontratos").submit(function (e) {
            e.preventDefault();
            var form = $(this);

            var formData = new FormData(form[0]);
            formData.append('pdf', $('#pdf')[0].files[0]);

            $.ajax({
                type: "POST",
                context: document.body,
                url: '{{route("contrato.crear")}}',
                processData: false,
                contentType: false,
                data: formData,
                beforeSend: function () {
                    cargando();
                },
                success: function (result) {
                    responseSuccess('Bien!!',result.message);
                    table.ajax.reload();
                    modalBs.modal('hide');
                },
                error: function (xhr, status) {
                    responseError(xhr);
                },
                complete: function (xhr, status) {
                    fincarga();
                }
            });
        });


    });


</script>