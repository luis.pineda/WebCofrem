<div id="modalcrearempresas">
    {{Form::open(['route'=>['empresa.crearp'], 'class'=>'form-horizontal', 'id'=>'crearempresas'])}}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Agregar empresa</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div style="margin-bottom: 15px;" class="row">
                <div class="col-sm-offset-1 col-sm-2">
                    <select class="form-control" name="tipo_documento" id="tipo_documento">
                        @foreach($tipoDocumentos as $tipo)
                            <option data-descripcion="{{$tipo->descripcion}}" value="{{$tipo->id}}">{{$tipo->equivalente}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-9">
                {{Form::text('nit', null ,['id'=>'nit','class'=>'form-control', "required", "maxlength"=>"10", "tabindex"=>"2", "onchange"=>"limpiar()","placeholder"=>"NUMERO DE IDENTIFICACIÓN TRIBUTARIA"])}} <!-- , "data-parsley-type"=>"number" -->
                </div>
            </div>

            <div class="form-group">
                <div class="widget-inline-box" align="center">
                    <button onclick="consultarWS()" type="button" id="botonConsultar"
                            class="btn btn-custom waves-effect waves-light">Consultar en SevenAs
                    </button>
                </div>
            </div>
            <div name="mensaje" id="mensaje" style="visibility: hidden;"><font color="red">No existe en SevenAs</font>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Razón social</label>
                <div class="col-md-9">
                    {{Form::text('razon_social', null ,['id'=>'razon_social', 'class'=>'form-control', "required", "maxlength"=>"120", "tabindex"=>"3"])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Representante Legal</label>
                <div class="col-md-9">
                    {{Form::text('representante_legal', null ,['id'=>'representante_legal', 'class'=>'form-control', "required", "maxlength"=>"40", "data-parsley-pattern"=>"^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ]+(\s*[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ]*)*[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ]+$", "tabindex"=>"4"])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Departamento</label>
                <div class="col-md-3">
                    {{Form::select("departamento_codigo",$departamentos,null,['class'=>'form-control', "tabindex"=>"5", 'id'=>'departamento'])}}
                </div>
                <label class="col-md-2 control-label">Ciudad</label>
                <div class="col-md-4">
                    <select name="municipio_codigo" id="municipio" tabindex="6" class="form-control">
                        <option>Seleccione...</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Dirección</label>
                <div class="col-md-9">
                    {{Form::text('direccion', null ,['id'=>'direccion', 'class'=>'form-control', "required", "maxlength"=>"40", "tabindex"=>"7"])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">E-mail</label>
                <div class="col-md-9">
                    {{Form::email('email', null ,['id'=>'email', 'class'=>'form-control', "required", "tabindex"=>"8"])}}
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-3 control-label">Teléfono</label>
                <div class="col-md-9">
                    {{Form::text('telefono', null ,['id'=>'telefono', 'class'=>'form-control', "required", "data-parsley-type"=>"number", "maxlength"=>"10", "tabindex"=>"9"])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Celular</label>
                <div class="col-md-9">
                    {{Form::text('celular', null ,['id'=>'celular', 'class'=>'form-control', "required", "data-parsley-type"=>"number", "maxlength"=>"10", "tabindex"=>"10"])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Tipo</label>
                <div class="col-md-9">
                    <!-- { {Form::select('tipo', [ 'T' => 'Tercero', 'A' => 'Afiliado'], 'T',['id'=>'tipo', 'class'=>'form-control', "tabindex"=>"11", "required", "readonly", "onmouseover"=>"this.disabled=true", "onmouseout"=>"this.disabled=false"])}} -->
                    {{Form::text('tipo', 'Tercero' ,['id'=>'tipo', 'class'=>'form-control', "required", "readonly", "tabindex"=>"11"])}}

                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-custom waves-effect waves-light">Guardar</button>
    </div>
    {{Form::close()}}
</div>

<script>

    var municipio = "";
    $(function () {

        $('#tip_docu').change(function(){

            $("#nit").attr("placeholder",$(this).find(':selected').data('descripcion'));
            limpiar();
        });

        setTimeout(function () {
            $('#nombre').focus();
        }, 1000);
        $("#crearempresas").parsley();
        $("#crearempresas").submit(function (e) {
            e.preventDefault();
            var form = $(this);
            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    cargando();
                },
                success: function (result) {
                    if (result.status) {
                        swal(
                            {
                                title: 'Bien!!',
                                text: result.message,
                                type: 'success',
                                confirmButtonColor: '#4fa7f3'
                            }
                        );
                    }
                    table.ajax.reload();
                    modalBs.modal('hide');
                },
                error: function (xhr, status) {
                    var mensajefinal;
                    var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                    mensajefinal = message + '\n';
                    mensajefinal += xhr.responseJSON.message+'\n';
                    if (xhr.status === 422) {
                        var errores="";
                        for (i = 0; i < xhr.responseJSON.data.length; i++) {
                            errores += xhr.responseJSON.data[i] + '\n';
                        }
                        mensajefinal += errores;
                        swal({
                            type: 'error',
                            title: 'Error...',
                            text: mensajefinal,
                        })
                    } else {
                        swal(
                            'Error!!',
                            mensajefinal
                            ,
                            'error'
                        )
                    }
                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    fincarga();
                }
            });
        });

        setTimeout(getMunicipios, '300');
        $("#departamento").change(function () {
            getMunicipios();
        });
    });


    function getMunicipios() {
        var dept = $("#departamento").val();
        $.get('{{route('municipios')}}', {data: dept}, function (result) {
            $('#municipio').html("");
            $.each(result, function (i, value) {
                $('#municipio').append($('<option>').text(value.descripcion).attr('value', value.codigo));
            });
            if(municipio !== ""){
                document.getElementById('municipio').value = municipio;
                municipio = "";
            }
        })
    }

    function limpiar() {
        document.getElementById("mensaje").style.visibility = "hidden";
        //document.getElementById('tipo').selectedIndex = "T";

        $('#tipo').attr('readonly', true);
        //$('#tipo option:not(:selected)').attr('disabled',true);
        document.getElementById('tipo').value = "Tercero";
        $('#razon_social').removeAttr('readonly');
        $('#representante_legal').removeAttr('readonly');
        $('#direccion').removeAttr('readonly');
        $('#email').removeAttr('readonly');
        $('#telefono').removeAttr('readonly');
        $('#celular').removeAttr('readonly');
        document.getElementById('razon_social').value = "";
        document.getElementById('representante_legal').value = "";
        document.getElementById('direccion').value = "";
        document.getElementById('email').value = "";
        document.getElementById('telefono').value = "";
        document.getElementById('celular').value = "";
    }

    function consultarWS() {
        var tipo = $("#tipo_documento").val();
        var num = $("#nit").val();
        $.ajax({
            method: "get",
            url: "{!!route('consultaraportante')!!}",
            data: {
                "tipo": tipo,
                "num": num
            },
            beforeSend: function () {
                cargando();
            },
            error: function (xhr, status) {
                console.log(xhr);
                var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                mensajefinal = message + '\n';
                mensajefinal += xhr.responseJSON.message+'\n';
                swal(
                    'Error!!',
                    mensajefinal
                    ,
                    'error'
                )
            },
            success: function (result) {

                if (result.data.tipo == "A") {
                    document.getElementById("mensaje").style.visibility = "hidden";
                    document.getElementById('razon_social').value = result.data.razon_social;
                    document.getElementById('representante_legal').value = result.data.representante_legal;
                    document.getElementById('direccion').value = result.data.direccion;
                    document.getElementById('email').value = result.data.email;
                    document.getElementById('telefono').value = result.data.telefono;
                    document.getElementById('celular').value = result.data.celular;
                    document.getElementById('tipo').value = "Afiliado";//data.tipo
                    document.getElementById('departamento').value = result.data.departamento_codigo;
                    getMunicipios();
                    municipio = result.data.municipio_codigo;
                    document.getElementById('municipio').value = result.data.municipio_codigo;
                    $('#razon_social').attr('readonly', true);
                    $('#representante_legal').attr('readonly', true);
                    $('#direccion').attr('readonly', true);
                    if (result.data.email == "NA")
                        $('#email').removeAttr('readonly');
                    else
                        $('#email').attr('readonly', true);
                    $('#telefono').attr('readonly', true);
                    if (result.data.celular == 0)
                        $('#celular').removeAttr('readonly');
                    else
                        $('#celular').attr('readonly', true);
                    $('#tipo').attr('readonly', true);
                }
                else {
                    limpiar();
                    document.getElementById("mensaje").style.visibility = "visible";

                }
            },
            complete: function (xhr, status) {
                fincarga();
            }
        });
    }

</script>